package com.mpakhomov.old.search

import com.mpakhomov.old.search.BinarySearch
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class BinarySearchTests extends AnyFunSuite with Matchers {

  test("Array(1, 2, 3, 4), 2 -> Some(1)") {
    BinarySearch.binarySearch(Array(1, 2, 3, 4), 2) shouldBe Some(1)
  }

  test("Array(1, 2, 3, 4), 5 -> None") {
    BinarySearch.binarySearch(Array(1, 2, 3, 4), 5) shouldBe None
  }

}
