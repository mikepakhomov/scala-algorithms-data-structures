package com.mpakhomov.old.leetcode

import com.mpakhomov.old.leetcode.Problem3_LongestSubstringWithoutRepeatingCharacters
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class Problem3_LongestSubstringWithoutRepeatingCharactersTests extends AnyFunSuite with Matchers {

  test("abcabcbb -> 3") {
    Problem3_LongestSubstringWithoutRepeatingCharacters.lengthOfLongestSubstring("abcabcbb") shouldBe 3
  }

  test("bbbbb -> 1") {
    Problem3_LongestSubstringWithoutRepeatingCharacters.lengthOfLongestSubstring("bbbbb") shouldBe 1
  }

  test("pwwkew -> 3") {
    Problem3_LongestSubstringWithoutRepeatingCharacters.lengthOfLongestSubstring("pwwkew") shouldBe 3
  }

  test("c -> 1") {
    Problem3_LongestSubstringWithoutRepeatingCharacters.lengthOfLongestSubstring("c") shouldBe 1
  }

  test("dvdf-> 3") {
    Problem3_LongestSubstringWithoutRepeatingCharacters.lengthOfLongestSubstring("dvdf") shouldBe 3
  }

  test("abba-> 2") {
    Problem3_LongestSubstringWithoutRepeatingCharacters.lengthOfLongestSubstring("abba") shouldBe 2
  }

}
