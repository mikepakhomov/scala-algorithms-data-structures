package com.mpakhomov.old.leetcode

import com.mpakhomov.old.leetcode.Problem7_ReverseInteger
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import org.scalatest.prop.TableDrivenPropertyChecks

class Problem7_ReverseIntegerTests extends AnyFunSuite with TableDrivenPropertyChecks with Matchers {

  val cases = Table(
    ("input", "result"),
    (123, 321),
    (-123, -321),
    (1534236469, 0),
    (-2147483648, 0), // Int.MinValue
    (-2147483647, 0), // Int.MinValue - 1
    (2147483647, 0), // Int.MaxValue
    (0, 0)
  )

  test("reverse") {
    forAll(cases) { (input, result) =>
      Problem7_ReverseInteger.reverse(input) shouldBe result
    }
  }

  test("reverseFast") {
    forAll(cases) { (input, result) =>
      Problem7_ReverseInteger.reverseFast(input) shouldBe result
    }
  }

  test("reverseFastRecursive") {
    forAll(cases) { (input, result) =>
      Problem7_ReverseInteger.reverseFastRecursive(input) shouldBe result
    }
  }
}