package com.mpakhomov.old.leetcode

import com.mpakhomov.old.leetcode.Problem1_TwoSum
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class Problem1_TwoSumTests extends AnyFunSuite with Matchers {

  Problem1_TwoSum.twoSum(Array(3, 3), 6) shouldBe Array(0, 1)
  Problem1_TwoSum.twoSum(Array(2, 7, 11, 15), 9) shouldBe Array(0, 1)

}
