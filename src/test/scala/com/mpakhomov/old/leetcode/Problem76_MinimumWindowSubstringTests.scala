package com.mpakhomov.old.leetcode

import com.mpakhomov.old.leetcode.Problem76_MinimumWindowSubstring
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class Problem76_MinimumWindowSubstringTests extends AnyFunSuite with Matchers {

  test("ADOBECODEBANC, ABC -> BANC") {
    Problem76_MinimumWindowSubstring.minWindow("ADOBECODEBANC", "ABC") shouldBe "BANC"
  }

  test("ANNDOBENCODEBANC, ABC -> BANC") {
    Problem76_MinimumWindowSubstring.minWindow("ANNDOBENCODEBANC", "BANC") shouldBe "BANC"
  }

  test("AABBCCABC, ABC -> ABC") {
    Problem76_MinimumWindowSubstring.minWindow("AABBCCABC", "ABC") shouldBe "CAB"
  }

  test("ABCAABBCC, ABC -> ABC") {
    Problem76_MinimumWindowSubstring.minWindow("ABCAABBCC", "ABC") shouldBe "ABC"
  }

  test("ABC, ABC -> ABC") {
    Problem76_MinimumWindowSubstring.minWindow("ABC", "ABC") shouldBe "ABC"
  }

  test("ABC, DEF -> ") {
    Problem76_MinimumWindowSubstring.minWindow("ABC", "DEF") shouldBe ""
  }

}
