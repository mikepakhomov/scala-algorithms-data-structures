package com.mpakhomov.old.leetcode

import com.mpakhomov.old.leetcode.Problem06_ZigZagConversion
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class Problem06_ZigZagConversionTests extends AnyFunSuite with Matchers {

  test("(PAYPALISHIRING, 3) -> PAHNAPLSIIGYIR") {
    Problem06_ZigZagConversion.convert(
      "PAYPALISHIRING",
      3
    ) shouldBe "PAHNAPLSIIGYIR"
  }

}
