package com.mpakhomov.old.leetcode

import com.mpakhomov.old.leetcode.Problem08_MyAtoi
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import org.scalatest.prop.TableDrivenPropertyChecks

class Problem08_MyAtoiTests extends AnyFunSuite with TableDrivenPropertyChecks with Matchers {

  val cases = Table(
    ("input", "result"),
    ("  123", 123),
    (" -123", -123),
    ("+123", 123),
    ("1534236469DDDDD", 1534236469),
    ("-2147483648", Int.MinValue), // Int.MinValue
    ("-2147483647", -2147483647), // Int.MinValue + 1
    ("-2147483649", Int.MinValue), // Int.MinValue - 1
    ("2147483647", Int.MaxValue), // Int.MaxValue
    ("2147483648", Int.MaxValue), // Int.MaxValue + 1
    ("0", 0),
    ("", 0),
    ("  \t\t", 0),
    ("    10522545459", Int.MaxValue)
  )

  test("myAtoi") {
    forAll(cases) { (input, result) =>
      Problem08_MyAtoi.myAtoi(input) shouldBe result
    }
  }
}
