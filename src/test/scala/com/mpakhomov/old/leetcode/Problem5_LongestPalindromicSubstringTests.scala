package com.mpakhomov.old.leetcode

import com.mpakhomov.old.leetcode.Problem5_LongestPalindromicSubstring
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class Problem5_LongestPalindromicSubstringTests extends AnyFunSuite with Matchers {

  test("""isPalindrome("abcba") -> true""") {
    Problem5_LongestPalindromicSubstring.isPalindrome("abcba") shouldBe true
  }

  test("""isPalindrome("abba") -> true""") {
    Problem5_LongestPalindromicSubstring.isPalindrome("abba") shouldBe true
  }

  test("""isPalindrome("abbac") -> false""") {
    Problem5_LongestPalindromicSubstring.isPalindrome("abbac") shouldBe false
  }

  test("""isPalindrome("a") -> true""") {
    Problem5_LongestPalindromicSubstring.isPalindrome("a") shouldBe true
  }

  test("""isPalindrome("") -> true""") {
    Problem5_LongestPalindromicSubstring.isPalindrome("") shouldBe true
  }

  test("babad -> bab or aba") {
    Array("bab", "aba") should contain (Problem5_LongestPalindromicSubstring.longestPalindromeDP("babad"))
  }

  test("cbbd -> bb") {
    Problem5_LongestPalindromicSubstring.longestPalindromeDP("cbbd") shouldBe "bb"
  }

  test("a -> a") {
    Problem5_LongestPalindromicSubstring.longestPalindromeDP("a") shouldBe "a"
  }

  test(" -> ") {
    Problem5_LongestPalindromicSubstring.longestPalindromeDP("") shouldBe ""
  }

  test("acfqfcb -> cfgfc") {
    Problem5_LongestPalindromicSubstring.longestPalindromeDP("acfgfcb") shouldBe "cfgfc"
  }

}
