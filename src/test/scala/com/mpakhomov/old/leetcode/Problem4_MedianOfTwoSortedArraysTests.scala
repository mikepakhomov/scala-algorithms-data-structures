package com.mpakhomov.old.leetcode

import com.mpakhomov.old.leetcode.Problem4_MedianOfTwoSortedArrays
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class Problem4_MedianOfTwoSortedArraysTests extends AnyFunSuite with Matchers {

  test("[1, 2], [3] -> 2.0") {
    Problem4_MedianOfTwoSortedArrays.findMedianSortedArrays(Array(1, 2), Array(3)) shouldBe 2.0
  }

  test("[1, 2], [3, 4] -> 2.5") {
    Problem4_MedianOfTwoSortedArrays.findMedianSortedArrays(Array(1, 2), Array(3, 4)) shouldBe 2.5
  }

  test("[3, 4], [1, 2] -> 2.5") {
    Problem4_MedianOfTwoSortedArrays.findMedianSortedArrays(Array(3, 4), Array(1, 2)) shouldBe 2.5
  }

  test("[3, 4], [1] -> 3.0") {
    Problem4_MedianOfTwoSortedArrays.findMedianSortedArrays(Array(3, 4), Array(1)) shouldBe 3.0
  }

  test("[3, 4], [] -> 3.5") {
    Problem4_MedianOfTwoSortedArrays.findMedianSortedArrays(Array(3, 4), Array()) shouldBe 3.5
  }

  test("[], [3, 4] -> 3.5") {
    Problem4_MedianOfTwoSortedArrays.findMedianSortedArrays(Array(), Array(3, 4)) shouldBe 3.5
  }

  test("[1, 2, 10], [4, 5] -> 4.0") {
    Problem4_MedianOfTwoSortedArrays.findMedianSortedArrays(Array(1, 2, 10), Array(4, 5)) shouldBe 4.0
  }

  test("[3, 8, 10], [1, 5] -> 5.0") {
    Problem4_MedianOfTwoSortedArrays.findMedianSortedArrays(Array(3, 8, 10), Array(1, 5)) shouldBe 5.0
  }

  test("[3, 8, 10], [1, 11] -> 8.0") {
    Problem4_MedianOfTwoSortedArrays.findMedianSortedArrays(Array(3, 8, 10), Array(1, 11)) shouldBe 8.0
  }

  test("[9, 10, 11], [1, 2, 3, 4, 5] -> 4.5") {
    Problem4_MedianOfTwoSortedArrays.findMedianSortedArrays(Array(9, 10, 11), Array(1, 2, 3, 4, 5)) shouldBe 4.5
  }

  test("[1, 2, 3, 4], [5, 6, 7, 8, 9, 10, 11, 12] -> 6.5") {
    Problem4_MedianOfTwoSortedArrays.findMedianSortedArrays(Array(1, 2, 3, 4), Array(5, 6, 7, 8, 9, 10, 11, 12)) shouldBe 6.5
  }

  test("[23, 26, 31, 35], [3, 5, 7, 9, 11, 16] -> 13.5") {
    Problem4_MedianOfTwoSortedArrays.findMedianSortedArrays(Array(23, 26, 31, 35), Array(3, 5, 7, 9, 11, 16)) shouldBe 13.5
  }


}
