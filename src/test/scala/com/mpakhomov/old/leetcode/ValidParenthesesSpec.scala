package com.mpakhomov.old.leetcode

import com.mpakhomov.old.leetcode.ValidParentheses
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class ValidParenthesesSpec extends AnyFunSuite with Matchers {
  ValidParentheses.isValid("()") shouldBe true
  ValidParentheses.isValid("()[]{}") shouldBe true
  ValidParentheses.isValid("(]") shouldBe false
  ValidParentheses.isValid( "([)]") shouldBe false
  ValidParentheses.isValid("{[]}") shouldBe true
  ValidParentheses.isValid("") shouldBe true
}
