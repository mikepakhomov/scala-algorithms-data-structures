package com.mpakhomov.old.leetcode

import com.mpakhomov.old.leetcode.Problem09_PalindromeNumber
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import org.scalatest.prop.TableDrivenPropertyChecks

class Problem09_PalindromeNumberTests
    extends AnyFunSuite
    with TableDrivenPropertyChecks
    with Matchers {

  val cases = Table(
    ("input", "result"),
    (123, false),
    (-121, false),
    (121, true),
    (1221, true),
    (12210, false),
    (12213, false),
    (1, true),
    (9, true),
    (10, false),
    (Int.MaxValue, false)
  )

  test("Palindrome number") {
    forAll(cases) { (input, result) =>
      Problem09_PalindromeNumber.isPalindrome(input) shouldBe result
    }
  }

}
