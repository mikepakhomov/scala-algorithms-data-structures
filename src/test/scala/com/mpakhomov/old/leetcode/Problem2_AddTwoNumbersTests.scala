package com.mpakhomov.old.leetcode

import com.mpakhomov.old.leetcode.Problem2_AddTwoNumbers
import com.mpakhomov.old.leetcode.Problem2_AddTwoNumbers.ListNode
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class Problem2_AddTwoNumbersTests extends AnyFunSuite with Matchers {

  test("342 + 465 == 807") {
    val l1 = new ListNode(2)
    l1.next = new ListNode(4)
    l1.next.next = new ListNode(3)

    val l2 = new ListNode(5)
    l2.next = new ListNode(6)
    l2.next.next = new ListNode(4)

    val result = Problem2_AddTwoNumbers.addTwoNumbers(l1, l2)
    assert(result.x === 7)
    assert(result.next.x === 0)
    assert(result.next.next.x === 8)
  }

  test("5 + 5 == 10") {
    val l1 = new ListNode(5)
    val l2 = new ListNode(5)

    val result = Problem2_AddTwoNumbers.addTwoNumbers(l1, l2)
    result.x shouldBe  0
    result.next.x shouldBe 1
  }

  test("18 + 0 == 18") {
    val l1 = new ListNode(8)
    l1.next = new ListNode(1)
    val l2 = new ListNode(0)

    val result = Problem2_AddTwoNumbers.addTwoNumbers(l1, l2)
    assert(result.x === 8)
    assert(result.next.x === 1)
  }

  test("81 + 0 == 81") {
    val l1 = new ListNode(1)
    l1.next = new ListNode(8)
    val l2 = new ListNode(0)

    val result = Problem2_AddTwoNumbers.addTwoNumbers(l1, l2)
    assert(result.x === 1)
    assert(result.next.x === 8)
  }

  test("89 + 1 == 90") {
    val l1 = new ListNode(9)
    l1.next = new ListNode(8)
    val l2 = new ListNode(1)

    val result = Problem2_AddTwoNumbers.addTwoNumbers(l1, l2)
    assert(result.x === 0)
    assert(result.next.x === 9)
  }

}
