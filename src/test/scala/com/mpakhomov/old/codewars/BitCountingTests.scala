package com.mpakhomov.old.codewars

import org.scalatest.funsuite.AnyFunSuite
import com.mpakhomov.old.codewars.BitCounting.countBits

class BitCountingTests extends AnyFunSuite {

  test("Samples") {
    assert(countBits(0) === 0)
    assert(countBits(4) === 1)
    assert(countBits(7) === 3)
    assert(countBits(9) === 2)
    assert(countBits(10) === 2)
  }
}
