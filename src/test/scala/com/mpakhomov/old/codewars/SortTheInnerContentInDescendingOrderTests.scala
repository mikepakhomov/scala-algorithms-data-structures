package com.mpakhomov.old.codewars

import com.mpakhomov.old.codewars.SortTheInnerContentInDescendingOrder.sortTheInnerContent
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import org.scalatest.prop.TableDrivenPropertyChecks

class SortTheInnerContentInDescendingOrderTests extends AnyFunSuite with TableDrivenPropertyChecks with Matchers {

  val cases = Table(
    ("input", "result"),
    ("sort the inner content in descending order", "srot the inner ctonnet in dsnnieedcg oredr"),
    ("wait for me", "wiat for me"),
    ("this kata is easy", "tihs ktaa is esay"),
    ("this  kata   is    easy", "tihs  ktaa   is    esay")
  )

  test("sortTheInnerContent") {
    forAll(cases) { (input, result) =>
      sortTheInnerContent(input) shouldBe result
    }
  }

}
