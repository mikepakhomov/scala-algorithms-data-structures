package com.mpakhomov.old.codility.lesson5

import com.mpakhomov.old.codility.lesson5.PassingCars
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class PassingCarsTests extends AnyFunSuite with Matchers {

  test("(0, 1, 0, 1, 1)") {
    PassingCars.solution(Array(0, 1, 0, 1, 1)) shouldBe 5
  }

}
