package com.mpakhomov.old.codility.lesson5

import com.mpakhomov.old.codility.lesson5.CountDiv
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class CountDivTests extends AnyFunSuite with Matchers {

  test("a = 6, b = 11, k = 2. solution = 3") {
    CountDiv.solution(6, 11, 2) shouldBe 3
  }

  test("a = 0, b = 0, k = 11. solution = 1") {
    CountDiv.solution(0, 0, 11) shouldBe 1
  }

  test("a = 0, b = 1, k = 11. solution = 1") {
    CountDiv.solution(0, 1, 11) shouldBe 1
  }

  // A = 11, B = 345, K = 17
  test("a = 11, b = 345, k = 17. solution = 20") {
    CountDiv.solution(11, 345, 17) shouldBe 20
  }

}
