package com.mpakhomov.old.codility.lesson5

import com.mpakhomov.old.codility.lesson5.GenomicRangeQuery
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class GenomicRangeQueryTests extends AnyFunSuite with Matchers {

  test("base test: s: CAGCCTA, p: (2, 5, 0), q: (4, 5, 6) => (2, 4, 1)") {
    GenomicRangeQuery.solution("CAGCCTA", Array(2, 5, 0), Array(4, 5, 6)) shouldBe Array(2, 4, 1)
  }

}
