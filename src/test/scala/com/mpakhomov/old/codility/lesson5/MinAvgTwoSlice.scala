package com.mpakhomov.old.codility.lesson5

import com.mpakhomov.old.codility.lesson5.MinAvgTwoSlice
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class MinAvgTwoSliceTests extends AnyFunSuite with Matchers {

  test("(4, 2, 2, 5, 1, 5, 8), solution = 1") {
    MinAvgTwoSlice.solution(Array(4, 2, 2, 5, 1, 5, 8)) shouldBe 1
  }

}
