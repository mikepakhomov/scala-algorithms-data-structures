package com.mpakhomov.old.codility.lesson3

import com.mpakhomov.old.codility.lesson3.PermMissingElem
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class PermMissingElemTests extends AnyFlatSpec with Matchers {

  trait Setup {
    val array: Array[Int]
    val answer: Int
    def test(): Unit = {
      it should s"give a solution $answer for array: ${array.mkString(", ")}" in {
        PermMissingElem.solution(array) shouldBe answer
      }
    }
  }

  new Setup {
    override val array: Array[Int] = Array(2, 3, 1, 5)
    override val answer: Int = 4
    test()
  }

}
