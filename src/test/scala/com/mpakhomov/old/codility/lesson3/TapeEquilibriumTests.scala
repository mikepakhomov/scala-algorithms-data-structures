package com.mpakhomov.old.codility.lesson3

import com.mpakhomov.old.codility.lesson3.TapeEquilibrium
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TapeEquilibriumTests extends AnyFlatSpec with Matchers {

  trait Setup {
    val array: Array[Int]
    val answer: Int
    def test(): Unit = {
      it should s"give a solution $answer for array: ${array.mkString(", ")}" in {
        TapeEquilibrium.solution(array) shouldBe answer
      }
    }
  }

  new Setup {
    override val answer: Int = 1
    override val array: Array[Int] = Array(3, 1, 2, 4, 3)
    test()
  }

}
