package com.mpakhomov.old.codility.unknown1

import com.mpakhomov.old.codility.unknown1.Tree._
import com.mpakhomov.old.codility.unknown1.VisibleNodes
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers


class VisibleNodesTests extends AnyFunSuite with Matchers {
  val tree = node(5,
                  node(3,
                    node(20),
                    node(21)),
                  node(10,
                    node(1)))

  test("result should be 5") {
    VisibleNodes.solution(tree) shouldBe 4
  }
}
