package com.mpakhomov.old.codility.unknown1

import com.mpakhomov.old.codility.unknown1.DecimalZip
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class DecomalZipTests extends AnyFunSuite with Matchers {

  test("12, 56 => 1526") {
    DecimalZip.solution(12, 56) shouldBe 1526
  }

  test("56, 12 => 5162") {
    DecimalZip.solution(56, 12) shouldBe 5162
  }

  test("12345, 678 => 16273845") {
    DecimalZip.solution(12345, 678) shouldBe 16273845
  }

  test("123, 67890 => 16273890") {
    DecimalZip.solution(123, 67890) shouldBe 16273890
  }

  test("123456, 67890 => 16273890") {
    DecimalZip.solution(123456, 67890) shouldBe -1
  }

}
