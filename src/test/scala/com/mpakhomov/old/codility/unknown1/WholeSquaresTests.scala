package com.mpakhomov.old.codility.unknown1

import com.mpakhomov.old.codility.unknown1.WholeSquares
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class WholeSquaresTests extends AnyFunSuite with Matchers {

  test("[4, 17]. solution = 3") {
    WholeSquares.solution(4, 17) shouldBe 3
  }

  test("[5, 5]. solution = 0") {
    WholeSquares.solution(5, 5) shouldBe 0
  }

  test("[5, 7]. solution = 0") {
    WholeSquares.solution(5, 7) shouldBe 0
  }

  test("[4, 4]. solution = 1") {
    WholeSquares.solution(4, 4) shouldBe 1
  }
}
