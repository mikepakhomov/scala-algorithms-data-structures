package com.mpakhomov.old.codility.unknown1

import com.mpakhomov.old.codility.unknown1.SwapSorted
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class SwapSortedTests extends AnyFunSuite with Matchers {

  test("(1, 5, 3, 3, 7). solution = 1") {
    SwapSorted.solution(Array(1, 5, 3, 3, 7)) shouldBe 1
  }

  test("(1, 3, 5, 3, 4). solution = 0") {
    SwapSorted.solution(Array(1, 3, 5, 3, 4)) shouldBe 0
  }

  test("(6, 5). solution = 1") {
    SwapSorted.solution(Array(6, 5)) shouldBe 1
  }

  test("(5). solution = 1") {
    SwapSorted.solution(Array(5)) shouldBe 1
  }

  test("(1, 5, 3). solution = 1") {
    SwapSorted.solution(Array(1, 5, 3)) shouldBe 1
  }

  test("(3, 5, 1). solution = 0") {
    SwapSorted.solution(Array(3, 5, 1)) shouldBe 0
  }

}
