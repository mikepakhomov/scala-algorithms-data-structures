package com.mpakhomov.old.codility.lesson6

import com.mpakhomov.old.codility.lesson6.MaxProductOfThree
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class MaxProductOfThreeTests extends AnyFunSuite with Matchers {

  test("(-5, 5, -5, 4) => 125") {
    MaxProductOfThree.solution(Array(-5, 5, -5, 4)) shouldBe 125
  }

  test("(-3, 1, 2, -2, 5, 6) => 60") {
    MaxProductOfThree.solution(Array(-3, 1, 2, -2, 5, 6)) shouldBe 60
  }

}
