package com.mpakhomov.old.codility.lesson6

import com.mpakhomov.old.codility.lesson6.Distinct
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class DistinctTests extends AnyFunSuite with Matchers {

  test("(2, 1, 1, 2, 3, 1) => 3") {
    Distinct.solution(Array(2, 1, 1, 2, 3, 1)) shouldBe 3
  }

}
