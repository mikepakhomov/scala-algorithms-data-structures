package com.mpakhomov.old.codility.lesson6

import com.mpakhomov.old.codility.lesson6.Triangle
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class TriangleTests extends AnyFunSuite with Matchers {

  test("(10, 2, 5, 1, 8, 20) => 1") {
    Triangle.solution(Array(10, 2, 5, 1, 8, 20)) shouldBe 1
  }

  test("(10, 50, 5, 1) => 0") {
    Triangle.solution(Array(10, 50, 5, 1)) shouldBe 0
  }

}
