package com.mpakhomov.old.codility.lesson1

import com.mpakhomov.old.codility.lesson1.{BinaryGap, BinaryGapBitManipulation}
import org.scalatest.flatspec.AnyFlatSpec

class BinaryGapTests extends AnyFlatSpec {

  type Solver = Int => Int
  case class BinaryGapSolution(solver: Solver, name: String)
  val solutions: List[BinaryGapSolution] = List(
    BinaryGapSolution(n => BinaryGap.solution(n), "BinaryGap"),
    BinaryGapSolution(n => BinaryGapBitManipulation.solution(n), "BinaryGapBitManipulation")
  )

  val n1 = 1041
  for (solution <- solutions) {
    s"${solution.name}: $n1" should "have gap 5" in {
      assert(solution.solver(n1) === 5)
    }
  }

}
