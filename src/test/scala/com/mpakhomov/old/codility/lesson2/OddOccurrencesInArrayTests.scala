package com.mpakhomov.old.codility.lesson2

import com.mpakhomov.old.codility.lesson2.OddOccurrencesInArray
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class OddOccurrencesInArrayTests extends AnyFlatSpec with Matchers {

  trait Setup {
    val array: Array[Int]
    val answer: Int
    def test(): Unit = {
      it should s"give a solution $answer for array: ${array.mkString(", ")}" in {
        OddOccurrencesInArray.solution(array) shouldBe answer
      }
    }
  }

  new Setup {
    val array = Array(9, 3, 9, 3, 9, 7, 9)
    val answer = 7
    test()
  }

  new Setup {
    val array = Array(1, 3, 3, 1, 5)
    val answer = 5
    test()
  }
}
