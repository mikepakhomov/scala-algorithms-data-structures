package com.mpakhomov.old.codility.lesson2

import com.mpakhomov.old.codility.lesson2.Solution
import org.scalatest.flatspec.AnyFlatSpec

class CyclicRotationTests extends AnyFlatSpec {

  type Answer = (Int, Array[Int])

  trait Setup {
    val answers: Array[Answer]
    val a: Array[Int]
  }


  new Setup {
    override val answers = Array[Answer](
      (0, Array(3, 8, 9, 7, 6)),
      (1, Array(6, 3, 8, 9, 7)),
      (2, Array(7, 6, 3, 8, 9)),
      (3, Array(9, 7, 6, 3, 8)),
      (4, Array(8, 9, 7, 6, 3)),
      (5, Array(3, 8, 9, 7, 6)),
      (6, Array(6, 3, 8, 9, 7))
    )

    override val a: Array[Int] = Array(3, 8, 9, 7, 6)

    for (answer <- answers) {
      val k = answer._1
      val result = answer._2
      s"a: (${a.mkString(", ")}), k = ${k}" should s"give ${result.mkString(", ")}" in {
        assert(Solution.solution(a, k) === result)
      }
    }
  }

  new Setup {
    override val answers = Array[Answer](
      (0, Array(3, 8)),
      (1, Array(8, 3)),
      (2, Array(3, 8)),
      (3, Array(8, 3))
    )

    override val a: Array[Int] = Array(3, 8)

    for (answer <- answers) {
      val k = answer._1
      val result = answer._2
      s"a: (${a.mkString(", ")}), k = ${k}" should s"give ${result.mkString(", ")}" in {
        assert(Solution.solution(a, k) === result)
      }
    }
  }

  new Setup {
    override val answers = Array[Answer](
      (0, Array(3)),
      (1, Array(3)),
      (2, Array(3)),
      (3, Array(3))
    )

    override val a: Array[Int] = Array(3)

    for (answer <- answers) {
      val k = answer._1
      val result = answer._2
      s"a: (${a.mkString(", ")}), k = ${k}" should s"give ${result.mkString(", ")}" in {
        assert(Solution.solution(a, k) === result)
      }
    }
  }

}
