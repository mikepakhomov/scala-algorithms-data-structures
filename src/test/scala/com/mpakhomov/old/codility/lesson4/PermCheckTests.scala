package com.mpakhomov.old.codility.lesson4

import com.mpakhomov.old.codility.lesson4.PermCheck
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class PermCheckTests extends AnyFlatSpec with Matchers {

  trait Setup {
    val array: Array[Int]
    val answer: Int
    def test(): Unit = {
      it should s"give a solution $answer for array: ${array.mkString(", ")}" in {
        PermCheck.solution(array) shouldBe answer
      }
    }
  }

  new Setup {
    val array = Array(4, 1, 3, 2)
    val answer = 1
    test()
  }

  new Setup {
    val array = Array(4)
    val answer = 0
    test()
  }

  new Setup {
    val array = Array(1)
    val answer = 1
    test()
  }

  new Setup {
    val array = Array(4, 3)
    val answer = 0
    test()
  }

  new Setup {
    val array = Array(4, 2)
    val answer = 0
    test()
  }

}
