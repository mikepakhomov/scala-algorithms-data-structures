package com.mpakhomov.old.codility.lesson4

import com.mpakhomov.old.codility.lesson4.MaxCounters
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class MaxCountersTests extends AnyFunSuite with Matchers {

  test("(3, 4, 4, 6, 1, 4, 4), n = 5 => (3, 2, 2, 4, 2)") {
    MaxCounters.solution(5, Array(3, 4, 4, 6, 1, 4, 4)) should equal (Array(3, 2, 2, 4, 2))
  }

  test("(1), n = 1 => (1)") {
    MaxCounters.solution(1, Array(1)) should equal (Array(1))
  }

}
