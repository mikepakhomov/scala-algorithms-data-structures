package com.mpakhomov.old.codility.lesson4

import com.mpakhomov.old.codility.lesson4.MissingInteger
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class MissingIntegerTests extends AnyFunSuite with Matchers {

  test ("(1, 3, 6, 4, 1, 2). solution = 5") {
    MissingInteger.solution(Array(1, 3, 6, 4, 1, 2)) shouldBe 5
  }
}
