package com.mpakhomov.old.codility.lesson4

import com.mpakhomov.old.codility.lesson4.SwapTheElements
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class SwapTheElementsTests extends AnyFunSuite with Matchers {
  val m = 100

  val arrayA = Array(1, 2, 3, 2)
  val arrayB = Array(2, 3, 3, 4)
  val result = true

  test(s"a: (${arrayA.mkString(", ")}), b: (${arrayB.mkString(", ")}) -> $result") {
    SwapTheElements.solution(arrayA, arrayB, m) shouldBe true
  }

}
