package com.mpakhomov.old.codility.lesson4

import com.mpakhomov.old.codility.lesson4.FrogRiverOne
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class FrogRiverOneTests extends AnyFunSuite with Matchers {

  test("(1, 3, 1, 4, 2, 3, 5, 4), x = 5. solution = 6") {
    FrogRiverOne.solution(5, Array(1, 3, 1, 4, 2, 3, 5, 4)) shouldBe 6
  }

}
