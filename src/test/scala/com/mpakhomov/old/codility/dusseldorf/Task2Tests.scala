package com.mpakhomov.old.codility.dusseldorf

import com.mpakhomov.old.codility.dusseldorf.Task2
import org.scalatest.funsuite.AnyFunSuite

class Task2Tests extends AnyFunSuite {

  test("equality") {
    val s = "thesame"
    val t = "thesame"
    assert(Task2.solution(s, t) === "NOTHING")
  }

  test("insert") {
    val s = "nice"
    val t = "niece"
    assert(Task2.solution(s, t) === "INSERT e")
  }

  test("delete") {
    val s = "dropX"
    val t = "drop"
    assert(Task2.solution(s, t) === "DELETE X")
  }

  test("swap") {
    val s = "form"
    val t = "from"
    assert(Task2.solution(s, t) === "SWAP o r")
  }

  test("impossible") {
    val s = "o"
    val t = "odd"
    assert(Task2.solution(s, t) === "IMPOSSIBLE")
  }

}
