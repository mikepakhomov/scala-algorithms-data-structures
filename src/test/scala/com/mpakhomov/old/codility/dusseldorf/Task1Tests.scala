package com.mpakhomov.old.codility.dusseldorf

import com.mpakhomov.old.codility.dusseldorf.Task1
import org.scalatest.flatspec.AnyFlatSpec

class Task1Tests extends AnyFlatSpec {

  val input = "we test coders"
  val result = "ew tset sredoc"

  s"$input" should s"give a solution: $result" in {
    assert(Task1.solution(input) === result)
  }
}
