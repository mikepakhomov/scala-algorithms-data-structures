package com.mpakhomov.old.codility.dusseldorf

import com.mpakhomov.old.codility.dusseldorf.Task3
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class Task3Tests extends AnyFunSuite with Matchers {

  test("(0), solution = 1") {
    Task3.solution(Array(0)) shouldBe 1
  }

  test("(1, 2), solution = 1") {
    Task3.solution(Array(1, 2)) shouldBe 1
  }

  test("(0, 1), solution = 2") {
    Task3.solution(Array(0, 1)) shouldBe 2
  }

  test("(0, 0, 0, 1, 0), solution = 5") {
    Task3.solution(Array(0, 0, 0, 1, 0)) shouldBe 5
  }

  test("(0, 0, 0, 1, 0, 1, 1), solution = 6") {
    Task3.solution(Array(0, 0, 0, 1, 0, 1, 1)) shouldBe 6
  }

  test("(5, 4, -3, 2, 0, 1, -1, 0, 2, -3, 4, -5), solution = 7") {
    Task3.solution(Array(5, 4, -3, 2, 0, 1, -1, 0, 2, -3, 4, -5)) shouldBe 7
  }

  test("(1, 0, 0, 1, -1), solution = 4") {
    Task3.solution(Array(1, 0, 0, 1, -1)) shouldBe 4
  }

}
