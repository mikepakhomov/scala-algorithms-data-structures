package com.mpakhomov.old.codility.marketplato

import com.mpakhomov.old.codility.marketplato.Task2Correct
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class Task2CorrectTests extends AnyFunSuite with Matchers {

  def countOnes(x: Int) = x.toBinaryString.count(_ == '1')
  def numToArray(x: Int): Array[Int] = {
    val bin = x.toBinaryString.reverse
    (for {
      (bit, i) <- bin.zipWithIndex if (bit == '1')
    } yield i).toArray
  }

  test("all numbers from 1 to 10 000") {
    for (i <- 1 to 10000) {
      val actual = Task2Correct.solution(numToArray(i))
      val expected = countOnes(i * 3)
//      println(s"i: $i, actual: $actual, expected: $expected")
      actual shouldBe expected
    }
  }

}
