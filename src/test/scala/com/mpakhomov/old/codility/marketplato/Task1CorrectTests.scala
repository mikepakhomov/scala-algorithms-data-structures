package com.mpakhomov.old.codility.marketplato

import com.mpakhomov.old.codility.marketplato.Task1
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class Task1CorrectTests extends AnyFunSuite with Matchers {

  test("test1") {
    Task1.solution(Array(40, 40, 100, 80, 20), Array(3, 3, 2, 2, 3), 3, 5, 200) shouldBe 6
  }

  test("test2") {
    Task1.solution(Array(60, 80, 40), Array(2, 3, 5), 5, 2, 200) shouldBe 5
  }

}