package com.mpakhomov.old.codility.marketplato

import com.mpakhomov.old.codility.marketplato.Task2
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class Task2Tests extends AnyFunSuite with Matchers {

  test("145 => 4") {
    Task2.solution(Array(1, 4, 5)) shouldBe 4
  }

  test("37 => 6") {
    Task2.solution(Array(0, 2, 5)) shouldBe 6
  }
}
