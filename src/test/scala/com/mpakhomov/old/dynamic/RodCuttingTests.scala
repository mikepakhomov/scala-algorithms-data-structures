package com.mpakhomov.old.dynamic

import com.mpakhomov.old.dynamic.RodCutting
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.funsuite.AnyFunSuite

trait RodCuttingTestsSetup {
  val prices = Array(1, 5, 8, 9, 10, 17, 17, 20, 24, 30)
  val optimalRevenues = Array(0, 1, 5, 8, 10, 13, 17, 18, 22, 25, 30)
  val firstCuts = Array(0, 1, 2, 3, 2, 2, 6, 1, 2, 3, 10)
  val optimalCuts = Array(
    List(0),
    List(1),
    List(2),
    List(3),
    List(2, 2),
    List(2, 3),
    List(6),
    List(1, 6),
    List(2, 6),
    List(3, 6),
    List(10)
  )
}

/** Created by mike on 11/12/16.
  */
class RodCuttingFunSuite extends AnyFunSuite with RodCuttingTestsSetup {

  for (n <- 0 until optimalRevenues.length) {
    test(s"memoizedCutRod: n = $n, result = ${optimalRevenues(n)}") {
      assert(
        RodCutting.memoizedCutRod(prices, n) === optimalRevenues(n),
        s"max revenue should be ${optimalRevenues(n)}"
      )
    }
  }

}

class RodCuttingFlatSpec extends AnyFlatSpec with RodCuttingTestsSetup {

  for (n <- 0 until optimalRevenues.length) {
    "bottomUpCutRod " should s"give a solution ${optimalRevenues(n)} when n = $n" in {
      assert(RodCutting.bottomUpCutRod(prices, n) === optimalRevenues(n))
    }
  }

  for (n <- 0 until optimalRevenues.length) {
    val result = optimalCuts(n).mkString(", ")
    it should s"produce $result when n = $n" in {
      val solution = RodCutting.bottomUpCutRodExtended(prices, n)
      val cuts = RodCutting.buildRodCuttingSolution(n, solution)
      assert(cuts === optimalCuts(n))
    }
  }

  for (n <- 0 until optimalRevenues.length) {
    val result = optimalCuts(n).mkString(", ")
    "bottomUpCutRodExtendedFun" should s"produce $result when n = $n" in {
      val solution = RodCutting.bottomUpCutRodExtendedFun(prices, n)
      val cuts = RodCutting.buildRodCuttingSolutionFun(n, solution)
      assert(cuts === optimalCuts(n))
    }
  }

}
