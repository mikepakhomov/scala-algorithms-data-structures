package com.mpakhomov.old.dynamic

import com.mpakhomov.old.dynamic.CoinChange
import com.mpakhomov.old.dynamic.CoinChange.Solution
import org.scalatest.flatspec.AnyFlatSpec

class CoinChangeTests extends AnyFlatSpec {
  // coins denominations
  val denoms = List(1, 5, 12, 25)

  val amount = 16
  val nrOfCoins = 4
  val solutionCoins = List(5, 5, 5, 1)
  "bottomUpCoinChange" should s"produce: number of coins: $nrOfCoins, $solutionCoins when denominations: ${denoms} and amount: $amount " in {
    assert(CoinChange.bottomUpCoinChange(denoms, amount) === Solution(nrOfCoins, solutionCoins))
  }
}
