package com.mpakhomov.old.sorting

import com.mpakhomov.old.sorting.{InsertionSort, MergeSort, SelectionSort, Sorting}
import org.scalacheck.Gen
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

trait SortingSetup {
  val sorters: List[Sorting] = List(InsertionSort, MergeSort, SelectionSort)
}

class SortingTests
    extends AnyFunSuite
    with SortingSetup
    with ScalaCheckPropertyChecks
    with Matchers {

  for (sorter <- sorters) {
    val sorterName = sorter.getClass.getSimpleName

    test(s"empty list. sorter: ${sorterName}") {
      val empty = List()
      sorter.sort(empty) shouldBe sorted
    }

    test(s"list with one element, sorter: ${sorterName}") {
      val xs = List(42)
      sorter.sort(xs) shouldBe sorted
    }

    val xs = List(2, 4, 1, 3, 8)
    test(s"list $xs, sorter: ${sorterName}") {
      sorter.sort(xs) shouldBe sorted
    }
  }

  val inputs = List(List(), List(1), List(1, 2, 3), List(2, 1, 3))

  val rows = for {
    list <- inputs
    sorter <- sorters
  } yield (list, sorter)

  val table = Table(
    ("list", "sorter"),
    rows: _*
  )

  test("table driven tests") {
    forAll(table) { (list, sorter) =>
      sorter.sort(list) shouldBe sorted
    }
  }

  test("scalacheck") {
    implicit val input = Gen.listOf(Gen.choose(-1000, 1000))
    val sorter = InsertionSort
    forAll(input) { list =>
      sorter.sort(list) shouldBe sorted
    }
  }

}
