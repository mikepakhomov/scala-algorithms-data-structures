package com.mpakhomov.old.sorting

import com.mpakhomov.old.sorting.{InsertionSort, MergeSort, SelectionSort, Sorting}
import org.scalacheck.Gen
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class ScalaCheckTests
    extends AnyFunSuite
    with ScalaCheckPropertyChecks
    with Matchers {

  val sorters: List[Sorting] = List(InsertionSort, MergeSort, SelectionSort)

  test("Scala Check Test example") {
    implicit val input = Gen.listOf(Gen.choose(-1000, 1000))
    forAll { (xs: List[Int]) =>
      for (sorter <- sorters) {
        sorter.sort(xs) shouldBe sorted
      }
    }
  }

}
