package com.mpakhomov.old.homeag24

import com.mpakhomov.old.homeag24.MinDistance
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class MinDistanceTests extends AnyFunSuite with Matchers {

  test("Seq(1, 7, 5, 2, 2) -> 0") {
    val xs = Seq(1, 7, 5, 2, 2)
    MinDistance.minDistance(xs) shouldBe 0
  }

  test("Functional: Seq(1, 7, 5, 2, 2) -> 0") {
    val xs = Seq(1, 7, 5, 2, 2)
    MinDistance.minDistanceFun(xs) shouldBe 0
  }

}
