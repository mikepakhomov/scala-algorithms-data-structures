package com.mpakhomov.old.riskident

import com.mpakhomov.old.riskident.OrderProcessor
import com.mpakhomov.old.riskident.OrderProcessor.Order
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import java.io._

class OrderProcessorTests extends AnyFunSuite with Matchers {

  def createInput00(): InputStream = {
    val input00 =
      """3
        |0 3
        |1 9
        |2 6
      """.stripMargin
    new ByteArrayInputStream(input00.getBytes)
  }

  def createInput01(): InputStream = {
    val input01 =
      """3
        |0 3
        |1 9
        |2 5
      """.stripMargin
    new ByteArrayInputStream(input01.getBytes)
  }

  test("readFile: valid input00") {
    val orders = OrderProcessor.readFile(createInput00()).get
    val expected = Array(Order(0, 3), Order(1, 9), Order(2, 6))
    orders should contain inOrderElementsOf (expected)
  }

  test("process: input00") {
    val baos = new ByteArrayOutputStream()
    val out = new PrintStream(baos)
    val orders = OrderProcessor.process(createInput00(), out)
    baos.toString shouldBe "9"
  }

  test("process: input01") {
    val baos = new ByteArrayOutputStream()
    val out = new PrintStream(baos)
    val orders = OrderProcessor.process(createInput01(), out)
    baos.toString shouldBe "8"
  }

}

