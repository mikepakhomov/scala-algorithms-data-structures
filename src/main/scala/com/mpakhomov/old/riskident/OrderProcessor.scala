package com.mpakhomov.old.riskident

import java.io.{InputStream, PrintStream}
import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import scala.util.{Failure, Success, Try}

object OrderProcessor {

  case class Order(ord_time: Int, cook_time: Int)

/**
  * @param in: An InputStream, which contains the following input:
  * A line containing a single number: The number of guests G,
  * Followed by G lines containing two numbers Oi and Di
  * separated by space.
  * There may be a trailing newline.
  * Oi is the ordering time for Gi, Di is the time it takes to bake Gi's pizza.
  * 0 <= G <= 100000
  * 0 <= Oi <= 1000000000
  * 1 <= Di <= 1000000000
  *
  * @param out: A PrintStream, which process writes the following output to:
  * A single line containing the integer part of the average
  * waiting time if the input is valid.
  * A single line starting with the words "Syntax error" and
  * an optional description otherwise.
  * There may be a trailing newline.
  */
  def process(in: InputStream, out: PrintStream): Unit = {
    readFile(in) match {
      case Success(orders) =>
        out.print(processOrders(orders))
      case Failure(t) =>
        out.print(s"Syntax error: ${t.getMessage}")
    }
  }

  private[riskident] def processOrders(orders: Array[Order]): Int = {
    var curOrder = 0
    var totalWaitTime = 0
    val queue = ArrayBuffer[Order](orders(curOrder))
    var curTime = 0
    while (curOrder != orders.size - 1 || queue.nonEmpty) {
      val optimal = chooseOptimal(curTime, queue)
      val order = queue.remove(optimal)
      totalWaitTime += curTime + order.cook_time - order.ord_time
      val timeWhenPizzaIsBaked = curTime + order.cook_time
      while (orders(curOrder).ord_time <= timeWhenPizzaIsBaked && curOrder != orders.size - 1) {
        if (curOrder + 1 < orders.size && orders(curOrder + 1).ord_time < timeWhenPizzaIsBaked) {
          curOrder += 1
          queue += orders(curOrder)
        }
      }
      curTime = timeWhenPizzaIsBaked
    }
    totalWaitTime / orders.size
  }

  private[riskident] def chooseOptimal(curTime: Int, queue: ArrayBuffer[Order]): Int = {
    val optimal = queue.zipWithIndex
      .map { case (order, i) => (curTime + order.cook_time - order.ord_time, i) }
      .minBy(_._1)._2
    optimal
  }


  private[riskident] def readFile(in: InputStream): Try[Array[Order]] = Try {
    val lines = Source.fromInputStream(in).getLines()
    val n = lines.next().trim().toInt
    val orders = ArrayBuffer[Order]()
    while (lines.hasNext && orders.size < n) {
      val line = lines.next()
      val parts = line.trim().split("\\s+").map(_.toInt)
      orders += Order(ord_time = parts(0), cook_time = parts(1))
    }
    if (orders.size < n)
      throw new IllegalAccessException(s"Insufficient number of orders: actual: ${orders.size}, should be ${n}")
    else
      orders.toArray
  }
}

