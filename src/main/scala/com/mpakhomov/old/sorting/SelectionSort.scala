package com.mpakhomov.old.sorting

object SelectionSort extends Sorting {

  // MP: TODO: take a look at
  // http://stackoverflow.com/questions/1672074/selection-sort-in-functional-scala
  // https://github.com/vkostyukov/scalacaster/tree/master/src/sort
  override def sort(xs: List[Int]): List[Int] =
    if (xs.isEmpty) List()
    else {
      val ys = minimum(xs)
      if (ys.tail.isEmpty)
        ys
      else
        ys.head :: sort(ys.tail)
    }

  def minimum(xs: List[Int]): List[Int] =
    xs.tail.foldLeft(List(xs.head)) {
//    (List(xs.head) /: xs.tail) {
      (ys: List[Int], x: Int) =>
        if (x < ys.head) (x :: ys)
        else (ys.head :: x :: ys.tail)
    }

}
