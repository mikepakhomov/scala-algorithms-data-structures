package com.mpakhomov.old.sorting

trait Sorting {
  def sort(xs: List[Int]): List[Int]
}
