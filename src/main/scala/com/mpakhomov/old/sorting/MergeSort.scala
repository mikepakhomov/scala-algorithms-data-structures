package com.mpakhomov.old.sorting

object MergeSort extends Sorting {
  def sort(xs: List[Int]): List[Int] = {
    def merge(xs1: List[Int], xs2: List[Int]): List[Int] =
      if (xs1.isEmpty) xs2
      else if (xs2.isEmpty) xs1
      else if (xs1.head < xs2.head) xs1.head :: merge(xs1.tail, xs2)
      else xs2.head :: merge(xs1, xs2.tail)

    val (xs1, xs2) = xs.splitAt(xs.length / 2)
    if (xs.length <= 1) xs
    else merge(sort(xs1), sort(xs2))
  }
}
