package com.mpakhomov.old.sorting

object InsertionSort extends Sorting {

  def sort(xs: List[Int]): List[Int] = xs match {
    case Nil => List()
    case x :: rest => insert(x, sort(rest))
  }

  def insert(x: Int, xs: List[Int]): List[Int] = xs match {
    case Nil => List(x)
    case y :: ys => if (x <= y) x :: xs else y :: insert(x, ys)
  }
}
