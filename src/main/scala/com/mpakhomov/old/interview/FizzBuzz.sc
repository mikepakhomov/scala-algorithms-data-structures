val fizzBuzz = Stream.from(1)
  .map(x => (x % 3, x % 5) match {
    case (0, 0) => "FizzBuzz"
    case (0, _) => "Fizz"
    case (_, 0) => "Buzz"
    case _ => x.toString
  })

fizzBuzz.take(20).foreach(println(_))


