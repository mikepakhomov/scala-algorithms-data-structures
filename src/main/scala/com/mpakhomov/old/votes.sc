val votes = Array("Alex", "Michael", "Harry", "Dave", "Alex", "Michael")


val votesMap = votes.groupBy(identity).mapValues(_.length)
val maxVotes = votesMap.maxBy(_._2)._2
val winners = votesMap
  .filter(_._2 == maxVotes)
  .map(_._1: String)


winners


