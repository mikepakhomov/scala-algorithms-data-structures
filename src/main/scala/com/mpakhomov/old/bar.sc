import scala.math.Integral.Implicits._

5 /% 2

import scala.annotation.tailrec
@tailrec
def toDigits(n: Int, acc: List[Int]): List[Int] = {
  import scala.math.Integral.Implicits._
  val (quotient, remainder) = n /% 10
  if (quotient == 0) remainder :: acc
  else toDigits(quotient, remainder :: acc)
}

@tailrec
def toNumber(digits: List[Int], mult: Int, acc: Int): Int = digits match {
  case Nil => acc
  case x :: tail => toNumber(tail, mult * 10, acc + x * mult)
}

val x = 1534236469
val digits = toDigits(x, List())
//digits.reverse
//toNumber(digits.reverse, 1, 0)
//toNumber(digits, 1, 0)
digits.reverse.foldRight((1L, 0L)) {
  case (x, (mult, acc)) => (mult * 10, x * mult + acc)
}
//Array.tabulate(digits.size)(10 * _)
//10 ** 2
Math.abs(-1)
Math.abs(Int.MinValue + 1)

def isDigit(c: Char) = c match {
  case '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' => true
  case _ => false
}

'9'.toInt


