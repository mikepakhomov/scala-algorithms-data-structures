package com.mpakhomov.old.codewars

object VowelCount {

  def getCount(inputStr: String): Int = {
    inputStr.foldLeft(0)((acc, c) => if (c == 'a' || c == 'e' || c == 'o' || c == 'u' || c == 'i') acc + 1 else acc)
  }

}
