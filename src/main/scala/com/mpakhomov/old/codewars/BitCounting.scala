package com.mpakhomov.old.codewars

import scala.annotation.tailrec

/**
  * Write a function that takes an integer as input, and returns the number of bits that are equal to one in the binary representation of that number. You can guarantee that input is non-negative.
  *
  * Example: The binary representation of 1234 is 10011010010, so the function should return 5 in this case
  */

object BitCounting {
  def countBits(n: Int): Int = {
    @tailrec
    def toBinary(n: Int, acc: List[Int]): List[Int] =
      if (n <= 0) acc
      else toBinary(n >> 1, (n & 1):: acc) // n >> 1 == n / 2, n & 1 - extract the last bit of an integer
    toBinary(n, List[Int]()).count(_ == 1)
  }
}
