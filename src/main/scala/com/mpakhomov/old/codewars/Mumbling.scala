package com.mpakhomov.old.codewars

object Mumbling {
    def accum(s: String) = {
      s.zipWithIndex.map { case (c, i) => c.toUpper + (c.toLower.toString * i) }.mkString("-")
    }

  // simple and dumb
//  def accum(s: String) = {
//    val sb = new StringBuilder
//    for ((c, i) <- s.zipWithIndex) {
//      sb += c.toUpper
//      sb ++= c.toLower.toString * i
//      sb += '-'
//    }
//    sb.deleteCharAt(sb.size - 1).toString()
//  }
}
