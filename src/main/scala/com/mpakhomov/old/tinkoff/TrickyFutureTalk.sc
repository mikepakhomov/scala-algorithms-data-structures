import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

val talk = Seq(
  Future.successful("я"),
  Future.failed(new RuntimeException(".")),
  Future.successful("немного"),
  Future.failed(new RuntimeException("src/test")),
  Future.successful("заикаюсь"),
  Future.failed(new RuntimeException("..."))
)
// should get Future[(Seq[String], Seq[Throwable])]

val w: Seq[Future[String]] = talk.map { f => f.recover{case e: Throwable => s"$e"} }

//talk.map{ f =>
//  f.map{ s => Right(s) }.recover { case e: Throwable => Left(e) }
//}

//talk.map { f =>
//  f.onComplete {
//    case Success(s) => s
//    case Failure(e) =>
//  }
//}

val res = Await.result(Future.sequence(w),1 second).partition { s =>
  s.startsWith("java.lang.")
}

//val g = talk.map( Success(_):Try[String] ).recover{
//  case t => Failure(t)
//}.map {
//  case Success(s) =>
//  case Failure(t) =>
//}









// получить Future[(Seq[Throwable], Seq[String])]

