import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

val talk = Seq(
  Future.successful("я"),
  Future.failed(new RuntimeException(".")),
  Future.successful("немного"),
  Future.failed(new RuntimeException("src/test")),
  Future.successful("заикаюсь"),
  Future.failed(new RuntimeException("..."))
)
// should get Future[(Seq[String], Seq[Throwable])]

// https://stackoverflow.com/questions/15775824/how-to-carry-on-executing-future-sequence-despite-failure
val res = Future.sequence(
  talk.map { f =>
    f.map(Success(_)).recover { case e: Throwable => Failure(e) }
  }
).map { seq =>
  seq.partition(_.isSuccess) match { case (s, f) =>
    (s.map(_.get), f.map(_.failed.get))
  }
}

Await.result(res, 1 second)

//val res = Future.sequence(
//  talk.map { f =>
//    f.map(Try(_))
//  }
//)
//.map { seq =>
//  seq.partition(t => t.isSuccess) match { case (s, f) =>
//    (s.map(_.get), f.map(_.failed.get) )
//  }
//}

//Await.result(res, 1 second)