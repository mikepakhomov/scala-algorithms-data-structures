val s = "Hello, Scala"

s.groupBy(_.toChar) // groupBy(identity(_))
 .mapValues(_.size)
 .toSeq
 .sortBy(-_._2)
 //.take(3)

