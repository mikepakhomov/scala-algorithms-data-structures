package com.mpakhomov.old.search

object BinarySearch {
  def binarySearch(xs: Array[Int], x: Int): Option[Int] = {
    var (lo, hi) = (0, xs.size - 1)
    while(lo <= hi) {
      val mid: Int = (lo + hi) / 2
      if (xs(mid) == x) return Some(mid)
      else if (x < xs(mid)) hi = mid - 1
      else lo = mid + 1
    }
    None
  }
}
