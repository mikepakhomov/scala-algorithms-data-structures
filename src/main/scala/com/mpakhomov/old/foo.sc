import scala.collection.mutable.{ArrayBuffer, ListBuffer}

val ab = ArrayBuffer[String]()
ab ++= Seq("a", "b", "c")
ab.remove(1)
ab

case class Order(ord_time: Int, cook_time: Int)

val orders = ArrayBuffer(Order(0, 2), Order(1, 1), Order(1, 1))

//val o = orders.minBy(_.cook_time)
//orders -= o

val curTime = 2
//
//orders.zipWithIndex
//  .map { case (order, i) => (curTime + order.cook_time - order.ord_time, i) }
//  .minBy(_._1)._2

orders.map { order => (curTime + order.cook_time - order.ord_time) }
orders.minBy { order => (curTime + order.cook_time - order.ord_time) }

val lb = ListBuffer[Int]()
lb += 1
lb += 2
lb
