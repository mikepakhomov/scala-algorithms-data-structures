package com.mpakhomov.old.homeag24

object MinDistance {

  def minDistance(xs: Seq[Int]): Int = {
    require(xs.size > 2)
    val sorted = xs.sorted
    var minDiff = Int.MaxValue
    for (i <- 1 until xs.size) {
      val diff = sorted(i) - sorted(i - 1)
      if (diff < minDiff) {
        minDiff = diff
      }
    }
    minDiff
  }

  def minDistanceFun(xs: Seq[Int]): Int = {
    require(xs.size > 2)
    val sorted = xs.sorted
    (1 until xs.size).reduce { (minDiff, idx) =>
      Math.min(sorted(idx) - sorted(idx - 1), minDiff)
    }
//    sorted.zipWithIndex.foldLeft(Int.MaxValue) { case (minDiff: Int, (element, idx)) =>
//      val diff = if (idx > 0) sorted(idx) - sorted(idx - 1) else minDiff
//      if (diff < minDiff) diff
//      else minDiff
//    }
  }

}
