package com.mpakhomov.old.graph

case class Edge[T](v: T, w: T)

abstract class Graph[T](val edges: List[Edge[T]]) {
  type AdjListWithMapping = (Vector[List[Int]], Map[T, Int])

  val adjListWithMapping = buildAdjListWithMapping(edges)
  val adjList: Vector[List[Int]] =adjListWithMapping._1
  val objToId: Map[T, Int] = adjListWithMapping._2
  val idToObj: Map[Int, T] = objToId.map(_.swap)

  protected def buildAdjListWithMapping(edges: List[Edge[T]]): AdjListWithMapping

  def adjInt(id: Int) = adjList(id)
  def adj(v: T): Option[Seq[T]] = objToId.get(v)
    .map(id => adjList(id))
    .map(ids => ids.map(idToObj(_)))

  def nrOfVertices: Int = adjList.size
  def nrOfEdges: Int = edges.size

  override def toString: String = {
    s"${nrOfVertices} vertices, ${nrOfEdges} edges" +
    adjList
      .zipWithIndex
      .map { case (xs, id) => s"""$id: ${xs.mkString(", ")}""" }
      .mkString(sys.props("line.separator"))
  }
}

//class UndirectedGraph[T](edges: List[Edge[T]]) extends Graph[T](edges) {
//  override protected def buildAdjListWithMapping(edges: List[Edge[T]]): (Vector[List[Int]], Map[T, Int]) = {
//    def loop(edges: List[Edge[Int]],  acc: Vector[Edge[Int]]) =
//  }
//}