package com.mpakhomov.old.primes

object Primes {

  // Sieving integral numbers
  //def sieve(s: Stream[Int]): Stream[Int] = s.head #:: sieve(s.tail.filter(_ % s.head != 0))

  // All primes as a lazy sequence
  //val primes = sieve(Stream.from(2))

  // Dumping the first five primes
  //primes.take(5).toList

  //def primes(n: Int): Set[Int] = {
  //  def sieve(n): List[Int] =
  //    s.head :: sieve(s.tail.filter(_ % s.head != 0))
  //  sieve(n).toSet
  //}

  def primes(n: Int): Set[Int] = {
    def sieve(s: Stream[Int]): Stream[Int] = s.head #:: sieve(s.tail.filter(_ % s.head != 0))
    sieve(Stream.from(2)).takeWhile(_ < n).toSet
  }

  //primes(10)

  primes(100).toList.sorted

}
