val a = "ab"
val b = "zsd"

def mergeStrings(a: String, b: String): String = {
  var i = 0
  val sb = new StringBuilder
  while (i < a.length && i < b.length) {
    sb.append(a(i))
    sb.append(b(i))
    i += 1
  }
  if (i < a.length) sb.append(a.substring(i))
  else if (i < b.length) sb.append(b.substring(i))
  sb.toString()
}

mergeStrings(a, b)
