package com.mpakhomov.old.leetcode

object Problem06_ZigZagConversion {

  def convert(s: String, numRows: Int): String = {
    if (numRows == 1) return s
    val array = Array.ofDim[Char](numRows, s.size)
    var (row, col, i) = (0, 0, 0)
    while (i < s.size) {
      while (row < numRows && i < s.size) {
        array(row)(col) = s(i)
        i += 1
        row += 1
      }
      col += 1
      if (numRows >= 2) {
        row = numRows - 2
        while (row > 0 && i < s.size) {
          array(row)(col) = s(i)
          i += 1
          row -= 1
          col += 1
        }
      }
    }

    (for {
      i <- 0 until numRows
      j <- 0 until s.size if array(i)(j) != '\u0000'
    } yield array(i)(j)).mkString
  }

}
