package com.mpakhomov.old.leetcode

object Problem7_ReverseInteger {

  def reverse(x: Int): Int = {
    import scala.annotation.tailrec

    def toDigits(x: Int): List[Int] = {
      @tailrec
      def toDigitsAux(n: Int, acc: List[Int]): List[Int] = {
        import scala.math.Integral.Implicits._
        val (quotient, remainder) = n /% 10
        if (quotient == 0) remainder :: acc
        else toDigitsAux(quotient, remainder :: acc)
      }
      toDigitsAux(x, List())
    }

//    @tailrec
//    def toNumberReversed(digits: List[Int], mult: Int, acc: Int): Int = digits match {
//      case Nil => acc
//      case x :: tail => toNumberReversed(tail, mult * 10, acc + x * mult)
//    }

    def toNumber(digits: List[Int]): Long = digits.foldRight((1L, 0L)) {
        case (x, (mult, acc)) => (mult * 10, x * mult + acc)
    }._2

    val digits = toDigits(x)
    val reversed = toNumber(digits.reverse)
    if (reversed > Int.MaxValue || reversed < Int.MinValue) 0
    else reversed.toInt
  }

  def reverseFast(x: Int): Int = {
    var rev = 0L
    var num = x
    while (num != 0) {
      rev = 10 * rev + num % 10
      num /= 10
      if (rev > Int.MaxValue || rev < Int.MinValue) return 0
    }
    rev.toInt
  }

  def reverseFastRecursive(x: Int): Int = {
    @scala.annotation.tailrec
    def reverseFastRecursiveAux(x: Int, acc: Long): Long =
      if (acc > Int.MaxValue || acc < Int.MinValue) 0
      else if (x == 0) acc
      else reverseFastRecursiveAux(x / 10, 10 * acc + x % 10)
    reverseFastRecursiveAux(x, 0L).toInt
  }

}
