package com.mpakhomov.old.leetcode

object Problem08_MyAtoi {
  def myAtoi(str: String): Int = {
    def toNumber(digits: Seq[Int]): Int = {
      var (num, mult) = (0L, 1L)
      for (d <- digits.reverse) {
        num += d * mult
        if (num > Int.MaxValue) { return Int.MaxValue }
        else if (num < Int.MinValue) { return Int.MinValue }
        mult *= 10
      }
      num.toInt
    }

    def myIsDigit(c: Char): Boolean = c match {
      case '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' => true
      case _ => false
    }

    val raw = str.trim
    if (raw.isEmpty) return 0

    // determine sign and chop it from the string
    val (sign, s) = raw(0) match {
      case x if x == '+' => (1, raw.substring(1))
      case x if x == '-' => (-1, raw.substring(1))
      case _ => (1, raw)
    }

    val digits = s.takeWhile(c => myIsDigit(c)).map( _ - 48) // 48 is ASCII for '0'
    toNumber(digits.map(sign * _))
  }
}
