package com.mpakhomov.old.leetcode

class Problem155_MinStack {

  class Stack[T] {
    private var stack = List[T]()
    def push(e: T): Unit = { stack = e :: stack  }
    def peek(): T = stack.head
    def pop(): T = { val h = stack.head; stack = stack.tail; h }
    def isEmpty(): Boolean = { stack.isEmpty }
  }

  private var (stack, minStack) = (new Stack[Int](), new Stack[Int]())

  def push(x: Int): Unit = {
    if (stack.isEmpty()) {
      stack.push(x)
      minStack.push(x)
    } else {
      val min = minStack.peek()
      if (x <= min) { minStack.push(x) }
      stack.push(x)
    }
  }

  def pop(): Unit =  {
    if (stack.isEmpty()) throw new NoSuchElementException("stack is empty")
    else {
      val min = minStack.peek()
      if (stack.peek() == min) { minStack.pop() }
      stack.pop()
    }
  }

  def top(): Int = {
    stack.peek()
  }

  def getMin(): Int = {
    minStack.peek()
  }
}
