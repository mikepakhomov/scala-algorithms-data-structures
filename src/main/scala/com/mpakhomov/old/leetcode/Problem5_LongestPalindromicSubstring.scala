package com.mpakhomov.old.leetcode

object Problem5_LongestPalindromicSubstring {

  def isPalindrome(s: String): Boolean = {
    val len = s.size
    for (i <- 0 until len / 2) {
      if (s(i) != s(len - 1 - i)) return false
    }
    true
  }

  def longestPalindromeBruteForce(s: String): String = {
    var longest = ""
    for (start <- 0 until s.size) {
      for (end <- start to s.size) {
        val substr = s.substring(start, end)
        if (isPalindrome(substr) && substr.size > longest.size) longest = substr
      }
    }
    longest
  }

  def longestPalindromeDP(s: String): String = {
    val n = s.length
    var (start, maxLen) = (0, 0)
    val dp = Array.ofDim[Boolean](n, n)

    for (i <- n - 1 to 0 by -1) {
      for (j <- i until n) {
        dp(i)(j) = (s(i) == s(j)) && (j - i < 3 || dp(i + 1)(j - 1))

        if (dp(i)(j) && j - i + 1 > maxLen) {
          start = i
          maxLen = j - i + 1
        }
      }
    }
    s.substring(start, start + maxLen)
  }
}
