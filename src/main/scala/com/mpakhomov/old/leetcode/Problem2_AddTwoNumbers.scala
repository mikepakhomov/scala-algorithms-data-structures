package com.mpakhomov.old.leetcode

/**
You are given two non-empty linked lists representing two non-negative integers.
The digits are stored in reverse order and each of their nodes contain a single digit.
Add the two numbers and return it as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
Output: 7 -> 0 -> 8
*/

object Problem2_AddTwoNumbers {

  /** Definition for singly-linked list. */
  class ListNode(var _x: Int = 0) {
    var next: ListNode = null
    var x: Int = _x
  }

  def addTwoNumbers(l1: ListNode, l2: ListNode): ListNode = {
    var l1Cur = l1 // cur element of list 1
    var l2Cur = l2 // cur element of list 2
    var carry = 0 // when sum > 10 we need to add 1
    val dummyHead = new ListNode(0) // we'll be chopped off anyway
    var cur: ListNode = dummyHead
    while (l1Cur != null || l2Cur != null) {
      val l = if (l1Cur != null) l1Cur.x else 0
      val r = if (l2Cur != null) l2Cur.x else 0
      val sum = (l + r + carry) % 10
      carry = (l + r + carry) / 10
      cur.next = new ListNode(sum)
      cur = cur.next
      if (l1Cur != null) { l1Cur = l1Cur.next }
      if (l2Cur != null) { l2Cur = l2Cur.next }
    }
    if (carry == 1) { cur.next = new ListNode(1) } // need to add 1, e.g. 5 + 5 == 10
    dummyHead.next
  }
}
