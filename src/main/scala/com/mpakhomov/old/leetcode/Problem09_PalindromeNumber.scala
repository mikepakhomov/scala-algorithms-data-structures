package com.mpakhomov.old.leetcode

object Problem09_PalindromeNumber {

  def isPalindrome(x: Int): Boolean = {
    // this is to avoid cases like 12210, which would yield true otherwise
    if (x < 0 || (x != 0 && x % 10 == 0)) return false
    var (l, r) = (x, 0L)
    while(l > r) {
      r = r * 10 + l % 10
      l /= 10
    }
    l == r || l == r / 10 // l == r / 10 to handle odd number of digits, e.g. 121, 1 == 12 / 10
  }

}
