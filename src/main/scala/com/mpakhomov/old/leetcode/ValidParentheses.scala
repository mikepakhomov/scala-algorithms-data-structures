package com.mpakhomov.old.leetcode

import scala.collection.mutable

object ValidParentheses {
  def isValid(s: String): Boolean = {
    val stack = mutable.Stack[Char]()
    val parentheses = Map(
      '(' -> ')',
      '{' -> '}',
      '[' -> ']'
    )
    for (c <- s) {
      if (parentheses.keySet.contains(c)) {
        stack.push(c)
      } else if (stack.isEmpty || parentheses.get(stack.pop()) != Some(c)) {
        return false
      }
    }
    stack.isEmpty
  }
}
