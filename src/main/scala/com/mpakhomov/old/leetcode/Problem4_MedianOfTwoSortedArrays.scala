package com.mpakhomov.old.leetcode

/*
  https://www.youtube.com/watch?v=LPFhl65R7ww
 */
object Problem4_MedianOfTwoSortedArrays {

  def findMedianSortedArrays(nums1: Array[Int], nums2: Array[Int]): Double = {
    // we assume that length of x is less than length of y. if this is not the case, let's swap them
    if (nums1.size > nums2.size) return findMedianSortedArrays(nums2, nums1)

    val x = nums1.size
    val y = nums2.size

    var (lo, hi) = (0, x)
    while(lo <= hi) {
      val partitionX: Int = (lo + hi) / 2
      val partitionY: Int = (x + y + 1) / 2 - partitionX

      val maxLeftX = if (partitionX == 0) Double.NegativeInfinity else nums1(partitionX - 1)
      val minRightX = if (partitionX == x) Double.PositiveInfinity else nums1(partitionX)

      val maxLeftY = if (partitionY == 0) Double.NegativeInfinity else nums2(partitionY - 1)
      val minRightY = if (partitionY == y) Double.PositiveInfinity else nums2(partitionY)

      if (maxLeftX <= minRightY && maxLeftY <= minRightX) {
        // found!
        if ((x + y) % 2 == 0) {
          return (Math.max(maxLeftX, maxLeftY) + Math.min(minRightX, minRightY)) / 2
        } else {
          return Math.max(maxLeftX, maxLeftY)
        }
      } else if (maxLeftX > minRightY) {
        // move partition boundary to the left
        hi = partitionX - 1
      } else {
        // move partition boundary to the right
        lo = partitionX + 1
      }
    }
    throw new IllegalArgumentException("wrong input data")
  }

}
