package com.mpakhomov.old.leetcode

import scala.collection.mutable

object Problem3_LongestSubstringWithoutRepeatingCharacters {

  def lengthOfLongestSubstring(s: String): Int = {
    var maxLen = 0
    val map = mutable.Map[Char, Int]()
    var start = 0
    for ((c, i) <- s.zipWithIndex) {
      if (map.contains(c)) {
        start = Math.max(map(c), start)
      }
      maxLen = Math.max(i - start + 1, maxLen)
      map += (c -> (i + 1))
    }
    maxLen
  }

}
