package com.mpakhomov.old.leetcode

object Problem21_MergeTwoSortedLists {
  class ListNode(var _x: Int = 0) {
    var next: ListNode = null
    var x: Int = _x
  }

  def mergeTwoLists(l1: ListNode, l2: ListNode): ListNode = {
    val head = new ListNode()
    var (merged, l, r) = (head, l1, l2)

    def advance(xs: ListNode): ListNode = { merged.next = new ListNode(xs.x); merged = merged.next; xs.next }

    while(l != null || r != null) {
      if (r == null) {
        l = advance(l)
      } else if (l == null) (
        r = advance(r)
      ) else if (l.x < r.x) {
        l = advance(l)
      } else {
        r = advance(r)
      }
    }
    head.next
  }
}

