package com.mpakhomov.old.leetcode

import scala.collection.mutable

object Problem76_MinimumWindowSubstring {

  def minWindow(s: String, t: String): String = {
    // map contains char and how many times this char appears in a given substring
    // map(c) can be > 0 only for characters in string t
    val map = mutable.Map[Char, Int]()
    for (c <- s) { map += (c -> 0) }
    for (c <- t) { if (map.contains(c)) map.update(c, map(c) + 1) else return "" }

    // counter denotes how many chars are needed to form a valid substring that contains all chars from t
    var (start, end, minStart, minLen, counter) = (0, 0, 0, Int.MaxValue, t.size)
    while (end < s.size) {
      val charAtEnd = s.charAt(end)
      if (map(charAtEnd) > 0) { counter -= 1 }
      map += (charAtEnd -> (map(charAtEnd) - 1))
      end += 1

      // inside valid window
      while (counter == 0) {
        val validSub = s.substring(start, end)
        println(s"valid substring: ${s.substring(0, start)}[ ${validSub} ]${s.substring(end, s.length)}" +
                s", length: ${validSub.length}")
        if (minLen > end - start) {
          minLen = end - start
          minStart = start
        }

        val charAtStart = s.charAt(start)
        map += (charAtStart -> (map(charAtStart) + 1))

        if (map(charAtStart) > 0) { counter += 1 } // when charAtStart is from string t
        start += 1
      }
    }
    return (if (minLen == Int.MaxValue) "" else s.substring(minStart, minStart + minLen))
  }

}
