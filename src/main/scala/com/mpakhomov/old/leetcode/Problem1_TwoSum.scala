package com.mpakhomov.old.leetcode

import scala.annotation.tailrec

/**
Given an array of integers, return indices of the two numbers such that they add up to a specific target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

Example:
Given nums = [2, 7, 11, 15], target = 9,

Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].
*/

object Problem1_TwoSum {

  def twoSum(nums: Array[Int], target: Int): Array[Int] = {
    val numWithIndex = nums.zipWithIndex.toMap

    @tailrec
    def twoSumHelper(i: Int): Array[Int] =
      if (i == nums.size - 1) Array[Int]()
      else if (numWithIndex.contains(target - nums(i)) && i != numWithIndex(target - nums(i))) Array(i, numWithIndex(target - (nums(i))))
      else twoSumHelper(i + 1)

    twoSumHelper(0)
  }

}
