val a = Array(2, 4, 5)
val n = 10

val leaves = Array.tabulate(n)(_ + 1)
val eaten = Array.fill(n)(false)

for (x <- a) {
  for (i <- x until n by x) {
    eaten(i) = true
  }
}

eaten.filter(_ == false).size - 1