package com.mpakhomov.old.dynamic

import scala.annotation.tailrec
import scala.collection.mutable.ListBuffer

object RodCutting {

  case class Solution(revenues: Array[Int], firstCut: Array[Int])

  /**
    * solves Rod Cutting problem using memoization approach.
    * direct translation from CLRS book
    *
    * @param prices an array of prices
    * @param n length of a rod
    * @return optimal revenue
    */
  def memoizedCutRod(prices: Array[Int], n: Int): Int = {
    def memoizedCutRodAux(prices: Array[Int], n: Int, revenues: Array[Int]): Int = {
      var q = 0
      if (revenues(n) >= 0) return revenues(n)
      for (i <- 0 until n)
        q = Math.max(q, prices(i) + memoizedCutRodAux(prices, n - 1 - i, revenues))
      revenues(n) = q
      revenues(n)
    }

    // max revenues, optimal solutions
    val revenues = Array.fill[Int](n + 1)(Int.MinValue)
    revenues(0) = 0
    memoizedCutRodAux(prices, n, revenues)
  }

  /**
    * solves Rod Cutting problem using bottom up approach
    * direct translation from CLRS book
    *
    * @param prices an array of prices
    * @param n length of a rod
    * @return optimal revenue
    */
  def bottomUpCutRod(prices: Array[Int], n: Int): Int = {
    // max revenues, optimal solutions
    val revenues = Array.fill[Int](n + 1)(Int.MinValue)
    revenues(0) = 0
    for (j <- 1 to n) {
      var q = 0
      for (i <- 0 until j)
        q = Math.max(q, prices(i) + revenues(j - 1 - i))
      revenues(j) = q
    }
    revenues(n)
  }

  /**
    * solves Rod Cutting problem using bottom up approach.
    * it also reconstructs solution details, i.e. it builds a list of optimal cuts
    * direct translation from CLRS book
    *
    * @param prices an array of prices
    * @param n length of a rod
    * @return optimal revenue
    */
  def bottomUpCutRodExtended(prices: Array[Int], n: Int): Solution = {
    // max revenues, optimal solutions
    val revenues = Array.fill[Int](n + 1)(Int.MinValue)
    revenues(0) = 0
    val sizes = revenues.map(identity)
    for (j <- 1 to n) {
      var q = 0
      for (i <- 0 until j) {
        if (prices(i) + revenues(j - 1 - i) > q) {
          q = prices(i) + revenues(j - 1 - i)
          sizes(j) = (i + 1)
        }
      }
      revenues(j) = q
    }
    Solution(revenues, sizes)
  }

  /**
    * shows how the rod should be cut to get the max revenues
    * @param n length of the rod
    * @param s solution: optimal revenues and cuts
    * @return a list that contains optimal cuts
    */
  def buildRodCuttingSolution(n: Int, s: Solution): List[Int] = {
    if (n == 0) List(0)
    else {
      var i = n
      val cuts = ListBuffer.empty[Int]
      while (i > 0) {
        cuts += s.firstCut(i)
        i -= s.firstCut(i)
      }
      //    println(s"n $n, cuts: " + cuts.mkString(", "))
      cuts.toList
    }
  }

  /**
    * solves Rod Cutting problem using bottom up approach.
    * it also reconstructs solution details, i.e. it builds a list of optimal cuts
    * re-written in more functional style. got rid of var etc
    *
    * @param prices an array of prices
    * @param n length of a rod
    * @return optimal revenue
    */
  def bottomUpCutRodExtendedFun(prices: Array[Int], n: Int): Solution = {
    // max revenues, optimal solutions
    val revenues = Array.fill[Int](n + 1)(Int.MinValue)
    revenues(0) = 0
    val sizes = revenues.map(identity)
    for (j <- 1 to n) {
      val (revenue, size) =
        (for (i <- 0 until j) yield (prices(i) + revenues(j - 1 - i), i + 1)).maxBy(_._1)
      revenues(j) = revenue
      sizes(j) = size
    }
    Solution(revenues, sizes)
  }

  /**
    * shows how the rod should be cut to get the max revenues
    * Re-written from CLRS to be in functional style
    *
    * @param n length of the rod
    * @param s solution: optimal revenues and cuts
    * @return a list that contains optimal cuts
    */
  def buildRodCuttingSolutionFun(n: Int, s: Solution): List[Int] = {
    @tailrec def loop(i: Int, acc: ListBuffer[Int]): ListBuffer[Int] = {
      if (i == 0) acc
      else loop(i - s.firstCut(i), acc += s.firstCut(i))
    }
    if (n == 0) List(0)
    else loop(n, ListBuffer.empty[Int]).toList
  }


}
