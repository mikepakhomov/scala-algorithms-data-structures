package com.mpakhomov.old.dynamic

import scala.annotation.tailrec
import scala.collection.mutable.ArrayBuffer

object CoinChange {
  case class Solution(count: Int, solutionCoins: List[Int])

  def bottomUpCoinChange(coins: List[Int], amount: Int): Solution = {
    val solutions = Array.fill[Int](amount + 1)(0)
    val lastCoins = ArrayBuffer[Int](0)
    for (amnt <- 1 to amount) {
      val (cnt, coin) = (for {coin <- coins if coin <= amnt} yield (1 + solutions(amnt - coin), coin)).minBy(_._1)
      solutions(amnt) = cnt
      lastCoins += coin
    }
    Solution(solutions(amount), solutionCoins(amount, lastCoins))
  }

  def solutionCoins(amount: Int, lastCoins: ArrayBuffer[Int]): List[Int] = {
    @tailrec def loop(amount: Int, acc: List[Int]): List[Int] =
      if (amount == 0) return acc
      else loop(amount - lastCoins(amount), lastCoins(amount) :: acc)
    loop(amount, List())
  }

  // MP: TODO
  // take a look at http://stackoverflow.com/questions/6638933/dynamic-programming-in-the-functional-paradigm
  // http://stackoverflow.com/questions/25129721/scala-memoization-how-does-this-scala-memo-work
}