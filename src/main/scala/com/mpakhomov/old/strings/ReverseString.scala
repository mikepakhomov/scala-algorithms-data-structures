package com.mpakhomov.old.strings

object ReverseString {

  def main(args: Array[String]): Unit = {
    val s = "qwerty"
    println(s.foldLeft(""){(acc, e) => e + acc})

    val xs = List(1, 2, 3, 4, 5)
    println(xs.foldLeft(List[Int]()){(acc, x) => x :: acc})
    println(reverseList(xs))

  }

  def reverseList[T](xs: List[T]): List[T] = xs match {
    case x :: xs => reverseList(xs) ::: List(x)
    case Nil => xs
  }

}
