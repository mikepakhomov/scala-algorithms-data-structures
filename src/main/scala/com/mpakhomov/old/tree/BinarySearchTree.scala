package com.mpakhomov.old.tree

/**
  * Created by mike on 11/5/16.
  */
class Node(var value: Int, var left: Node, var right: Node, var parent: Node) {
  def add(newValue: Int): Unit =
    if (newValue <= value) {
      if (left == null) left = new Node(newValue, null, null, this)
      else left.add(newValue)
    }
    else {
      if (right == null) right = new Node(newValue, null, null, this)
      else right.add(newValue)
    }

  override def toString: String = {
    val l = if (left != null) left.toString else "."
    val r = if (right != null) right.toString else "."
    "{" + l + value + r + "}"
  }
}


class BinarySearchTree {
  private var _root: Node = null
  def root = _root

  def inOrder(node: Node): Unit = {
    if (node != null) {
      inOrder(node.left)
      println(node.value)
      inOrder(node.right)
    }
  }

  def treeMinimum(x: Node): Int =
    if (x.left == null) x.value
    else treeMinimum(x.left)

  def treeMaximum(x: Node): Int =
    if (x.right == null) x.value
    else treeMaximum(x.right)

  def treeSearch(x: Node, key: Int): Option[Node] = {
    if (x == null) return None
    if (x.value == key) return Some(x)
    if (key <= x.value) treeSearch(x.left, key)
    else treeSearch(x.right, key)
  }


  def treeSuccessor(x: Node): Option[Int] = {
    if (x.right != null) Some(treeMinimum(x.right))
    else {
      var cur = x
      var y = cur.parent
      while (y != null && cur == y.right) {
        cur = y
        y = y.parent
      }
      if (y == null) None else Some(y.value)
    }
  }

  def treePredecessor(x: Node): Option[Int] = {
    if (x.left != null) Some(treeMaximum(x.right))
    else {
      var cur = x
      var y = cur.parent
      while (y != null && cur == y.left) {
        cur = y
        y = y.parent
      }
      if (y == null) None else Some(y.value)
    }
  }
}

object BinarySearchTree {
  def apply(values: Int*): BinarySearchTree = {
    val tree = new BinarySearchTree
    if (values.length > 0) tree._root = new Node(values(0), null, null, null)
    for (v <- values.drop(1)) tree._root.add(v)
    tree
  }

  def main(args: Array[String]): Unit = {
    val t = BinarySearchTree(15, 6, 18, 17, 20, 3, 7, 13, 3, 2, 4, 9)
    println(t.root.toString)
//    t.inOrder(t.root)
    println(t.treeMaximum(t.root))
    println(t.treeMinimum(t.root))
    val n15 = t.treeSearch(t.root, 15).get
    println(t.treeSuccessor(n15))
    val n13 = t.treeSearch(t.root, 13).get
    println(t.treeSuccessor(n13))
    val n17 = t.treeSearch(t.root, 17).get
    println(t.treePredecessor(n17))
  }
}


