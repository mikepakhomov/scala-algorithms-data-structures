package com.mpakhomov.old.codility.marketplato

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object Task1 {

  case class WeightAndFloor(weight: Int, floor: Int)

  def solution(a: Array[Int], b: Array[Int], m: Int, x: Int, y: Int): Int = {
    def canAddMore(totalWeight: Int, nrOfPersons: Int, newPerson: WeightAndFloor, maxWeight: Int, maxSize: Int): Boolean =
      totalWeight + newPerson.weight < maxWeight && nrOfPersons + 1 <= maxSize

    def countNrOfStops(load: Seq[WeightAndFloor]): Int =
      load.map(_.floor).toSet.size + 1

    val queue = mutable.Queue[WeightAndFloor]()
    queue ++= (a zip b).map { case (w, f) => WeightAndFloor(w, f) }
    var load = ArrayBuffer[WeightAndFloor]()
    var totalWeight = 0
    var nrOfPersons = 0
    var nrOfStops = 0
    while (!queue.isEmpty) {
      while (!queue.isEmpty && canAddMore(totalWeight, nrOfPersons, queue.front, y, x)) {
        val newPerson = queue.dequeue()
        load += newPerson
        nrOfPersons += 1
        totalWeight += newPerson.weight
      }
      nrOfStops += countNrOfStops(load.toSeq)
      totalWeight = 0
      nrOfPersons = 0
      load.clear()
    }
    nrOfStops
  }
}
