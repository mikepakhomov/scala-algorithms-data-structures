package com.mpakhomov.old.codility.marketplato

import scala.annotation.tailrec

object Task2Correct {

  def solution(a: Array[Int]): Int = {

    def removeDups(xs: List[Int]): List[Int] = {
      @tailrec
      def removeDupsHelper(h: Int, rest: List[Int], acc: List[Int]): List[Int] = rest match {
        case Nil => h :: acc
        case x :: xs =>
          if (h != x) removeDupsHelper(x, xs, h :: acc)
          else (x + 1) :: acc ::: xs
      }

      if (xs.size <= 1) xs
      else removeDupsHelper(xs.head, xs.tail, List())
    }

    def hasDuplicates(xs: List[Int]) = xs.distinct.size != xs.size

    @tailrec
    def sum(intermediate: List[Int]): List[Int] =
      if(hasDuplicates(intermediate)) sum(removeDups(intermediate).sorted)
      else intermediate

    /**
      * idea: 3 * k = 2 * k + k
      * (1) multiplication by 2 is just a shifting bits one position to the right
      * (2) then we need to sum up two numbers in binary format
      */
    val k = a.toList
    val k2 = k.map(_ + 1) // 2 * k
    val k3 = sum((k ++ k2).sorted) // 3 * k
    k3.length
  }
}
