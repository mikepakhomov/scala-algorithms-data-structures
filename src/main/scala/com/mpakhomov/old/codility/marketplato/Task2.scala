package com.mpakhomov.old.codility.marketplato

object Task2 {
  def solution(a: Array[Int]): Int = {
    def countOnes(b: Byte): Int = Integer.toBinaryString(b & 0xFF).count(_ == '1')
    val k = (for {
      num <- a
    } yield BigInt(2).pow(num)).sum
    val k3 = k * 3
    val bytes = k3.toByteArray
    bytes.foldLeft(0){ (count, b) => count + countOnes(b) }
 }
}
