package com.mpakhomov.old.codility.lesson4

object PermCheck {

  def solution(a: Array[Int]): Int = {
    val count = new Array[Int](a.length)
    for (i <- 0 until a.length) {
      if (a(i) < 1 || a(i) > a.length) return 0
      count(a(i) - 1) = 1
    }
    if (count.contains(0)) 0
    else 1
  }

}
