package com.mpakhomov.old.codility.lesson4

object FrogRiverOne {

  def solution(x: Int, a: Array[Int]): Int = {
    val path = scala.collection.mutable.Set[Int]()
    for (i <- 0 until a.length) {
      path += a(i)
      if (path.size == x) return i
    }
    -1
  }

}
