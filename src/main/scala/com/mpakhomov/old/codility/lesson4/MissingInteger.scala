package com.mpakhomov.old.codility.lesson4

object MissingInteger {

  def solution(a: Array[Int]): Int = {
    val ints = new Array[Boolean](a.length)
    for (i <- 0 until a.length) if (a(i) > 0 && a(i) <= a.length) ints(a(i) - 1) = true
    for (i <- 0 until a.length) if (!ints(i)) return i + 1
    a.length + 1
  }

}
