package com.mpakhomov.old.codility.lesson4

object SwapTheElements {

  def solution(a: Array[Int], b: Array[Int], m: Int): Boolean = {
    def counting(array: Array[Int], m: Int): Array[Int] = {
      val count = new Array[Int](m + 1)
      for (a <- array) count(a) += 1
      count
    }

    val sumA: Long = a.foldLeft(0L)(_ + _)
    val sumB: Long = b.foldLeft(0L)(_ + _)
    if (sumA == sumB) return true
    val isDiffOdd = (sumB - sumA) % 2 == 1
    if (isDiffOdd) return false
    val d = (sumB - sumA) / 2
    val count = counting(a, m)
    for (bb <- b) {
      if (bb - d >= 0 && bb - d <= m && count((bb - d).toInt) > 0) return true
    }
    false
  }

}
