package com.mpakhomov.old.codility.lesson4

object MaxCounters {

  def solution(n: Int, a: Array[Int]): Array[Int] = {
    val result = new Array[Int](n)
    var max = 0
    for (e <- a) {
      if (e > 0 && e < result.length + 1) {
        result(e - 1) += 1
        max = math.max(max, result(e - 1))
      } else if (e == result.length + 1) {
//        result.transform(_ => max)
        for (i <- 0 until result.length) result(i) = max
      }
    }
    result
  }

}
