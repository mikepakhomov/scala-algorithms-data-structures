package com.mpakhomov.old.codility.lesson2

object OddOccurrencesInArray {

  def solution(a: Array[Int]): Int =
    a.reduce(_ ^ _)
//    a.reduceLeft((acc, x) => acc ^ x)
}
