package com.mpakhomov.old.codility.lesson2

object Solution {
  def solution(a: Array[Int], k: Int): Array[Int]  =
    if (a.size == 0 || a.size == 1) a
    else rotateK(a, k)

  def rotate1(a: Array[Int]): Array[Int] = {
    val n = a.size
    val b = new Array[Int](n)
    b(0) = a(n - 1)
    Array.copy(a, 0, b, 1, n - 1)
    b
  }

  def rotateK(a: Array[Int], k: Int): Array[Int] =
    if (k % (a.length) == 0) a
    else {
    val n = a.size
    val shift = k % a.length
    val b = new Array[Int](n)
    Array.copy(a, n - shift, b, 0, shift)
    Array.copy(a, 0, b, shift, n - shift)
    b
  }

}