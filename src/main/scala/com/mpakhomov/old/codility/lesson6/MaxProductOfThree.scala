package com.mpakhomov.old.codility.lesson6

object MaxProductOfThree {

  def solution(a: Array[Int]): Int = {
    java.util.Arrays.sort(a)
    Math.max(a(0) * a(1) * a(a.length - 1), a(a.length - 1) * a(a.length - 2) * a(a.length - 3))
  }
}
