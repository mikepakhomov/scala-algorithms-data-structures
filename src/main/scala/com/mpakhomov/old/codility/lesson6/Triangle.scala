package com.mpakhomov.old.codility.lesson6

object Triangle {

  def solution(a: Array[Int]): Int = {
    java.util.Arrays.sort(a)
    for (i <- 0 until a.length - 2)
      if (a(i).toLong + a(i + 1).toLong > a(i + 2).toLong) return 1
    0
  }

}
