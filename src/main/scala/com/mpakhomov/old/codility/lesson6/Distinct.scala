package com.mpakhomov.old.codility.lesson6

object Distinct {

  def solution(a: Array[Int]): Int = {
    if (a.length <= 1) return a.length
    java.util.Arrays.sort(a)
    a.sliding(2).foldLeft(1)((acc, pair) => if (pair(0) != pair(1)) acc + 1 else acc)
  }

}
