package com.mpakhomov.old.codility.unknown1

object SwapSorted {


  def solution(a: Array[Int]): Int = {

    def isSorted(a: Array[Int]): Boolean = {
      if (a.length < 1) return true
      for (i <- 0 until a.length - 1) if (a(i) > a(i + 1)) return false
      true
    }

    def swap(i: Int, j: Int): Unit = {
      val tmp = a(i)
      a(i) = a(j)
      a(j) = tmp
    }

    if (a.length <= 2 || isSorted(a)) return 1

    for (i <- 0 until a.length - 1) {
      for (j <- i + 1 until a.length) {
        swap(i, j)
        if (isSorted(a)) return 1
        swap(j, i)
      }
    }
    0
  }

}
