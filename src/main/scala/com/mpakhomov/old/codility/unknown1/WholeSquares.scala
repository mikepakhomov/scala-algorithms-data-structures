package com.mpakhomov.old.codility.unknown1

object WholeSquares {

  def solution(a: Int, b: Int): Int = {
    def within(x: Int, a: Int, b: Int): Boolean = x >= a && x <= b

    val start = math.sqrt(a).ceil.toInt
    var i = 0
    while (within((start + i) * (start + i), a, b)) i += 1
    i
  }

}
