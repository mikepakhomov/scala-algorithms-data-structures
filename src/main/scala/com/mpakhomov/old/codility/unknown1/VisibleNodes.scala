package com.mpakhomov.old.codility.unknown1

// very nice representation of binary tree:
// https://blog.adilakhter.com/2015/06/24/inverting-a-binary-tree-with-scala/
sealed trait Tree[+A]
case object EmptyTree extends Tree[Nothing]
case class Node[A](value: A , left: Tree[A], right: Tree[A]) extends Tree[A]

object Tree {
  def empty[A]: Tree[A] = EmptyTree
  def node[A](value: A, left: Tree[A] = empty, right: Tree[A] = empty): Tree[A] =
    Node(value, left, right)
}

object VisibleNodes {

  def max(xs: List[Int]): Int =
    if (xs.isEmpty) Int.MinValue
    else xs.max

  def count(t: Tree[Int], path: List[Int]): Int = {
    t match {
      case EmptyTree => 0
      case Node(value, l, r) =>
        val newPath = value :: path
        val visibleNodes: Int = if (value >= max(path)) 1 else 0
        visibleNodes + count(l, newPath) + count(r, newPath)
    }
  }

  def solution(t: Tree[Int]): Int = {
    count(t, List())
  }

}
