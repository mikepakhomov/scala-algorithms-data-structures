package com.mpakhomov.old.codility.unknown1

object DecimalZip {

  def solution(a: Int, b: Int): Int = {
    def digits(a: Int): Seq[Int] = a.toString.map(_.asDigit)
    def toNumber(xs: Seq[Int]): Long = xs.foldLeft(0L)((acc, x) => acc * 10 + x)
    val aDigits = digits(a)
    val bDigits = digits(b)
    val zipped = new Array[Int](aDigits.size + bDigits.size)
    var j = 0
    for (i <- 0 until math.max(aDigits.size, bDigits.size)) {
      if (i < aDigits.size) {
        zipped(j) = aDigits(i)
        j += 1
      }
      if (i < bDigits.size) {
        zipped(j) = bDigits(i)
        j += 1
      }
    }
    val num = toNumber(zipped)
    if (num <= 100000000) num.toInt else -1
  }

}
