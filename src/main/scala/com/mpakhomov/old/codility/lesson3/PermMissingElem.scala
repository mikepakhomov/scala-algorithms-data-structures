package com.mpakhomov.old.codility.lesson3

object PermMissingElem {
  def solution(a: Array[Int]): Int = {
    // sum of elements 1 .. N + 1
    val sum1ToNPlus1: Long = (a.length + 1L) * (a.length + 2L) / 2
    val sum: Long = a.foldLeft(0L)(_ + _)
    (sum1ToNPlus1 - sum).toInt
  }
}
