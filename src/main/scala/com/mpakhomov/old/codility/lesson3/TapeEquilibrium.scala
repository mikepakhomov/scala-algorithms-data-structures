package com.mpakhomov.old.codility.lesson3

object TapeEquilibrium {
  def solution(a: Array[Int]): Int = {
    var sum1 = 0L
    var sum2 = a.foldLeft(0L)(_ + _)
    var minDiff = Integer.MAX_VALUE
    for (i <- 1 until a.length) {
      sum1 += a(i - 1)
      sum2 -= a(i - 1)
      val diff = math.abs(sum1 - sum2).toInt
      minDiff = math.min(diff, minDiff)
    }
    minDiff
  }
}
