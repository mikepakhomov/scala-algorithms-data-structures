package com.mpakhomov.old.codility.lesson1

/**
  * Created by mike on 11/10/16.
  */
object BinaryGap {
  def solution(n: Int): Int = {
    val binString: String = n.toBinaryString
    var maximumGap = 0
    var gap = 0
    var inGap = false
    for (c <- binString) {
      c match {
        case '0' =>
          gap += 1
          if (!inGap) inGap = true
        case '1' =>
          if (inGap) {
            if (gap > maximumGap) maximumGap = gap
            inGap = false
            gap = 0
          }
      }
    }
    maximumGap
  }
}

object BinaryGapBitManipulation {
  def solution(n: Int): Int = {
    var maximumGap = 0
    var gap = 0
    var inGap = false
    var i = 1

    while ((n >> i) != 0) {
      val lsb = (n >> i) & 0x1 // least significant bit
      lsb match {
        case 0 =>
          gap += 1
          if (!inGap) inGap = true
        case 1 =>
          if (inGap) {
            if (gap > maximumGap) maximumGap = gap
            inGap = false
            gap = 0
          }
      }
      i += 1
    }
    maximumGap
  }
}

/**
  * Purely-functional solution
  */
object BinaryGapFp {
  // MP: TODO
  def solution(n: Int): Int = ???
}
