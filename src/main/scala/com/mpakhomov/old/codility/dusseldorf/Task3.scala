package com.mpakhomov.old.codility.dusseldorf

object Task3 {

  import Math.signum

  def solution(a: Array[Int]): Int = {
    var maxSliceLength = 0
    var curSliceLength = 0
    var nonZeroStartOfSlice: Option[Int] = None
    var numberOfConsecutiveZeroes = 0
    for (i <- 0 until a.length) {
      if (a(i) == 0) {
        curSliceLength += 1
        // we need to count number of consecutive zeroes for a tricky case like (1, 0, 0, 1, -1)
        numberOfConsecutiveZeroes += 1
      } else if (a(i) != 0) {
        if (nonZeroStartOfSlice.isEmpty) {
          nonZeroStartOfSlice = Some(i)
          curSliceLength += 1
        } else {
          // the sign should be according to the first non-zero element in the slice
          val nonZeroStartOfSliceIndex = nonZeroStartOfSlice.get
          val shouldHaveTheSameSign: Boolean = (nonZeroStartOfSliceIndex % 2) == (i % 2)
          if (shouldHaveTheSameSign && (signum(a(nonZeroStartOfSliceIndex))) == signum(a(i))) curSliceLength += 1
          else if (!shouldHaveTheSameSign && (signum(a(nonZeroStartOfSliceIndex))) != signum(a(i))) curSliceLength += 1
          else {
            // alternating slice has just finished
            maxSliceLength = Math.max(curSliceLength, maxSliceLength)
            curSliceLength = 1 + numberOfConsecutiveZeroes
            nonZeroStartOfSlice = Some(i)
          }
        }
        numberOfConsecutiveZeroes = 0
      }
    }
    Math.max(maxSliceLength, curSliceLength)
  }
}
