package com.mpakhomov.old.codility.dusseldorf

object Task1 {

  // reverse every word in s string
  def solution(s: String): String =
    s.split(" ").map(_.reverse).mkString(" ")

}
