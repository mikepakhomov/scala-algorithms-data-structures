package com.mpakhomov.old.codility.dusseldorf

object Task2 {

  type Transformation = (String, String) => String

    def solution(s: String, t: String): String = {
      val transformations = List[Transformation](eq, swap, ins, del)
      for (transformation <- transformations) {
        val result = transformation(s, t)
        if (!result.isEmpty) return result
      }
      "IMPOSSIBLE"
    }

    val eq: Transformation = (s, t) => if (s == t) "NOTHING" else ""

    def ins(s: String, t: String): String = {
      if (t.length - s.length != 1) return ""
      for (c <- s)
        for (i <- 0 to s.length) {
          if (new StringBuilder(s).insert(i, c).toString() == t) return s"INSERT ${c}"
        }
      ""
    }

  def del(s: String, t: String): String = {
    if (s.length - t.length != 1) return ""
    for (i <- 0 until s.length) {
      if (new StringBuilder(s).deleteCharAt(i).toString() == t) return s"DELETE ${s(i)}"
    }
    ""
  }

  def swap(s: String, t: String): String = {
    if (s.length != t.length) return ""
    for (i <- 0 until s.length - 1)
      for (j <- (i + 1) until s.length) {
        if (s(i) != s(j) && swap(s, i, j) == t) return s"SWAP ${s(i)} ${s(j)}"
      }
    ""
  }

  def swap(s: String, i: Int, j: Int): String = {
    val sb = new StringBuilder(s)
    sb.setCharAt(i, s(j))
    sb.setCharAt(j, s(i))
    sb.toString()
  }

}
