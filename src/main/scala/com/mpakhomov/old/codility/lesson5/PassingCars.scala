package com.mpakhomov.old.codility.lesson5

object PassingCars {

  def solution(a: Array[Int]): Int = {
    var passingCars = 0
    var nrOfZeroes = 0
    for (e <- a) {
      if (e == 0) nrOfZeroes += 1
      else {
        passingCars += nrOfZeroes
        if (passingCars > 1000000000) return -1
      }
    }
    passingCars
  }

}
