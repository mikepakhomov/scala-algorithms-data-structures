package com.mpakhomov.old.codility.lesson5

object GenomicRangeQuery {

  def solution(s: String, p: Array[Int], q: Array[Int]): Array[Int] = {
    def minInRange(counts: Array[Array[Int]], i: Int, j: Int): Int = {
      if (counts(0)(j + 1) - counts(0)(i) > 0) 1
      else if (counts(1)(j + 1) - counts(1)(i) > 0) 2
      else if (counts(2)(j + 1) - counts(2)(i) > 0) 3
      else 4
    }

    val counts = Array.ofDim[Int](4, s.length + 1)
    var sumOfA, sumOfC, sumOfG, sumOfT = 0
    for ((c, i) <- s zip (1 to s.length + 1)) {
      c match {
        case 'A' => sumOfA += 1
        case 'C' => sumOfC += 1
        case 'G' => sumOfG += 1
        case 'T' => sumOfT += 1
      }
      counts(0)(i) = sumOfA
      counts(1)(i) = sumOfC
      counts(2)(i) = sumOfG
      counts(3)(i) = sumOfT
    }
    val results = new Array[Int](p.length)
    for (((pp, qq), i) <- (p zip q).zipWithIndex) {
      results(i) = minInRange(counts, pp, qq)
    }
    results
  }

}
