package com.mpakhomov.old.codility.lesson5

object MinAvgTwoSlice {

  def solution(a: Array[Int]): Int = {
    // NB!: minimum slice could be only of length 2 or 3
    implicit val prefixSum = new Array[Int](a.length + 1)
    def slice(x: Int, y: Int)(implicit prefixSum: Array[Int]): Double = prefixSum(y + 1) - prefixSum(x)
    def avg2(x: Int): Double = slice(x, x + 1) / 2
    def avg3(x: Int): Double = slice(x, x + 2) / 3


    for (i <- 1 until a.length + 1) prefixSum(i) = prefixSum(i - 1) + a(i - 1)
    var minIndex = a.length - 2
    var minSlice = Double.MaxValue
    for (i <- a.length - 2 to 0 by -1) {
      val av2 = avg2(i)
      val av3 = if (i == a.length - 2) Double.MaxValue else avg3(i)
      val min = math.min(av2, av3)
      if (min <= minSlice) {
        minSlice = min
        minIndex = i
      }
    }
    minIndex
  }

}
