package com.mpakhomov.old.codility.lesson5

object CountDiv {

  def solution(a: Int, b: Int, k: Int): Int = {
    if (a % k == 0) 1 + (b - a) / k
    else (b - a + a % k) / k
  }

}
