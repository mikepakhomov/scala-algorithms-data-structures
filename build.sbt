name := """scala-algorithms-data-structures"""

version := "1.0"

scalaVersion := "2.13.13"

//resolvers += "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.2.19" % "test",
//  "org.specs2" %% "specs2-core" % "4.0.2" % "test",
  "org.scalatestplus" %% "scalacheck-1-18" % "3.2.19.0" % "test",
  "org.scalacheck" %% "scalacheck" % "1.18.1" % "test"
)

scalacOptions in Test ++= Seq("-Yrangepos")

//enablePlugins(PackPlugin)
